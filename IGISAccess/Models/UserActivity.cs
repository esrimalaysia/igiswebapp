﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGISAccess.Models
{
    public class UserActivityQueryString
    {
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string role { get; set; }
        public string layer { get; set; }
        public string groupby { get; set; }
        public string user { get; set; }
    }

    public class UserActivityInsertParam
    {
        public string username { get; set; }
        public string role { get; set; }
        public DateTime date { get; set; }
        public string layer { get; set; }
        public int inserts { get; set; }
        public int updates { get; set; }
        public int deletes { get; set; }
    }
}
