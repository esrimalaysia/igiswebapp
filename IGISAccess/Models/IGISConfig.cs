﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGISAccess.Models
{
    public class IGISConfig
    {
        public string ArcgisSvrUrl { get; set; }
        public string ArcgisSvrPsaUsr { get; set; }
        public string ArcgisSvrPsaPswd { get; set; }
        public string AdminConnectionString { get; set; }
        public string InsertActivityUrl { get; set; }
        public string EdmsQueryLink { get; set; }
        public string BrainsAssetNoQueryLink { get; set; }
        public string BrainsSanQueryLink { get; set; }
        public string IgisConnectionString { get; set; }
        public string LAQueryUrl { get; set; }
        public int TokenExpiryMinutes { get; set; }
        public string PortalUsername { get; set; }
        public string PortalPassword { get; set; }
        public string PortalUrl { get; set; }
        public string ManholeQueryUrl { get; set; }
        public string SewerlineQueryUrl1 { get; set; }
        public string SewerlineQueryUrl2 { get; set; }
        public string UpdateCEMSUrl { get; set; }
        public string RestComplaintUrl { get; set; }

    }
}
