﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IGISAccess.Models
{
    public class Role
    {
        string _name;
        public string RoleName
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string label
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }
        //public ICollection<Privilege> Privileges { get; set; }
    }

    public class User
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public ICollection<Role> Roles { get; set; }
    }
}
