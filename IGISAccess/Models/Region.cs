﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IGISAccess.Models
{
    public class Region
    {
        [JsonProperty(PropertyName = "region")]
        public string RegionName { get; set; }

        [JsonProperty(PropertyName = "filter")]
        public IEnumerable<RegionFilter> RegionFilters { get; set; }
    }

    public class RegionFilter
    {
        [JsonProperty(PropertyName = "field")]
        public string Field { get; set; }

        [JsonProperty(PropertyName = "values")]
        public IEnumerable<string> Values { get; set; }
    }

    public class LocalAuthority
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
    }
}
