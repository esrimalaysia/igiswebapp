﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IGISAccess.Models
{
    public class Privilege
    {
        [JsonProperty(PropertyName = "rolename")]
        public string RoleName { get; set; }
        public int RoleId { get; set; }

        public string AppName { get; set; }
        public int AppId { get; set; }

        [JsonProperty(PropertyName = "layers")]
        public IEnumerable<LayerPrivilege> Layers { get; set; }

        [JsonProperty(PropertyName = "webmapRef")]
        public string WebMapReference { get; set; }

        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "webmap")]
        public string WebMap { get; set; }
        public List<string> LocalAuthorities { get; set; }
    }

    public class LayerPrivilege
    {
        [JsonProperty(PropertyName = "layer")]
        public string Layer { get; set; }

        [JsonProperty(PropertyName = "canView")]
        public bool CanView { get; set; }

        [JsonProperty(PropertyName = "canEdit")]
        public bool CanEdit { get; set; }
    }
}
