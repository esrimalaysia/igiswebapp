﻿using IGISAccess.ArcgisAccess;
using IGISAccess.ViewModels;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace IGISAccess.Utility
{
    public static class Utils
    {
        public static double GetJsTimestampNow(int minutesDelayed)
        {
            return Math.Round(DateTime.UtcNow.AddMinutes(minutesDelayed)
                .Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
        }

    }
}