﻿using IGISAccess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace IGISAccess.ArcgisAccess
{
    public interface IArcgisSignInManager
    {
        Task<ArcgisSignInResult> CheckPasswordSignInAsync(string username, string password, bool getPortalToken = false);
    }

    public class ArcgisSignInResult
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success
        { get; set; }

        [JsonProperty(PropertyName = "errorReason")]
        public string ErrorReason
        { get; set; }

        [JsonProperty(PropertyName = "token")]
        public string Token
        { get; set; }

        [JsonProperty(PropertyName = "role")]
        public string Role
        { get; set; }

        [JsonProperty(PropertyName = "roles")]
        public List<string> Roles
        { get; set; }

        [JsonProperty(PropertyName = "expire")]
        public long ExpireTicks
        { get; set; }

        [JsonProperty(PropertyName = "portaltoken")]
        public string PortalToken
        { get; set; }
    }
    public class TokenResult : TokenJson
    {
        public TokenResult() { }
        public TokenResult(TokenJson token)
        {
            Success = token.Token == null ? false : true;
            Token = token.Token;
            Expires = token.Expires;
            long epochTicks;
            Int64.TryParse(token.Expires, out epochTicks);
            ExpireTicks = epochTicks;
            var ticks = ((epochTicks * 10000) + 621355968000000000); // convert Javascript ticks into .Net ticks
            ExpirationDate = new DateTime(ticks);
            if (token.ErrorStatus != null)
            {
                ErrorReason = string.Join("\n", token.ErrorMessages);
            }
        }
        public bool Success
        { get; set; }
        public string ErrorReason
        { get; set; }
        public DateTime ExpirationDate
        { get; set; }
        public long ExpireTicks
        { get; set; }
    }

    public class TokenJson
    {
        [JsonProperty(PropertyName = "token")]
        public string Token
        { get; set; }

        [JsonProperty(PropertyName = "expires")]
        public string Expires
        { get; set; }

        [JsonProperty(PropertyName = "messages")]
        public IEnumerable<string> ErrorMessages
        { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string ErrorStatus
        { get; set; }

        [JsonProperty(PropertyName = "code")]
        public int code
        { get; set; }
    }

    public class ArcgisSignInManager : IArcgisSignInManager
    {
        private readonly IGISConfig _igisConfig;

        class RolesJson
        {
            [JsonProperty(PropertyName = "roles")]
            public List<string> Roles
            { get; set; }
            [JsonProperty(PropertyName = "hasMore")]
            public bool HasMore
            { get; set; }
        }

        public ArcgisSignInManager(IGISConfig appConfig)
        {
            this._igisConfig = appConfig;
        }

        private HttpClient CreateNewHttpClient(string url)
        {
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {//ignore ssl issues
                    return true;
                };
            //base address needs to end with a '/'
            string arcgisServerUrl = url[url.Length - 1] == '/' ? url : url + '/'; 
            HttpClient client = new HttpClient(handler)
            {
                BaseAddress = new Uri(arcgisServerUrl)
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));

            return client;
        }

        public async Task<ArcgisSignInResult> CheckPasswordSignInAsync(string username, string password, bool getPortalToken = false)
        {
            HttpClient client = CreateNewHttpClient(_igisConfig.ArcgisSvrUrl);
            TokenResult tokenResult = await GetTokenFromArcgisServer(client, username, password, _igisConfig.TokenExpiryMinutes);

            if (tokenResult.Success)
            {//get roles associated with user
                //Need to get separate token with an admin user to get roles associated 
                TokenResult adminToken = await GetTokenFromArcgisServer(client, _igisConfig.ArcgisSvrPsaUsr, _igisConfig.ArcgisSvrPsaPswd, 10);
                if (!adminToken.Success)
                {
                    return new ArcgisSignInResult()
                    {
                        Success = false,
                        ErrorReason = "Error getting roles, likely due to incorrect arcgis server admin user credentials.",
                        Token = tokenResult.Token,
                    };
                }

                List<string> roles = new List<string>();
                if (username == _igisConfig.ArcgisSvrPsaUsr)
                {
                    roles.Add("HQ_Edit"); //automatic hq edit role for psa
                }
                else
                {
                    roles = await GetUserRolesFromArcgisServer(client, username, adminToken.Token);
                }

                if (roles.Count == 0)
                {
                    return new ArcgisSignInResult()
                    {
                        Success = false,
                        ErrorReason = "No roles found for user: " + username
                    };
                }
                ArcgisSignInResult returnResult = new ArcgisSignInResult()
                {
                    Success = tokenResult.Success,
                    ErrorReason = tokenResult.ErrorReason,
                    Token = tokenResult.Token,
                    ExpireTicks = (long)tokenResult.ExpireTicks,
                    Role = roles != null && roles.Count > 0 ? roles[0] : "",//Associate only first role with user, expected only one
                    Roles = roles
                };
                if (getPortalToken)
                {
                    TokenResult portalToken = await GetTokenFromArcgisPortal(_igisConfig.PortalUsername,
                        _igisConfig.PortalPassword, _igisConfig.TokenExpiryMinutes);
                    returnResult.PortalToken = portalToken.Token;
                }
                return returnResult;
            }
            else
            {
                return new ArcgisSignInResult()
                {
                    Success = tokenResult.Success,
                    ErrorReason = tokenResult.ErrorReason,
                };
            }
        }

        private async Task<TokenResult> GetTokenFromArcgisServer(HttpClient client, string username, string password, int minutesExpire)
        {
            var reqParams = new Dictionary<string, string>();
            reqParams.Add("username", username);
            reqParams.Add("password", password);
            reqParams.Add("client", "requestip");
            reqParams.Add("expiration", minutesExpire.ToString());
            reqParams.Add("f", "json");
            HttpResponseMessage response = null;

            try
            {
                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, "admin/generateToken");
                msg.Content = new FormUrlEncodedContent(reqParams);
                msg.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                response = await client.SendAsync(msg);
            }
            catch (Exception ex)
            {
                return new TokenResult()
                {
                    Success = false,
                    ErrorReason = "Failed to connect to ArcGIS server: " + ex.Message
                };
            }

            if (!response.IsSuccessStatusCode)
            {
                return new TokenResult()
                {
                    Success = false,
                    ErrorReason = "Failed to get token: Code: " + response.StatusCode + " : " + response.ReasonPhrase
                };
            }
            try
            {
                response.EnsureSuccessStatusCode();
                string json = await response.Content.ReadAsStringAsync();
                if (json.Contains("Could not access any server machines"))
                {
                    return new TokenResult()
                    {
                        Success = false,
                        ErrorReason = "Cannot Login. ArcGIS Server is not running."
                    };
                }

                TokenJson token = JsonConvert.DeserializeObject<TokenJson>(json);
                return new TokenResult(token) { };
            }
            catch (HttpRequestException ex)
            {
                return new TokenResult() { Success = false, ErrorReason = "Http Request Exception: " + ex.Message };
            }
            catch (JsonSerializationException ex)
            {
                return new TokenResult() { Success = false, ErrorReason = "Json serialization exception: " + ex.Message };
            }
            catch (Exception ex)
            {
                return new TokenResult() { Success = false, ErrorReason = "Exception: " + ex.Message };
            }
        }

        private async Task<List<string>> GetUserRolesFromArcgisServer(HttpClient client, string username, string token)
        {
            var reqParams = new Dictionary<string, string>();
            reqParams.Add("username", username);
            reqParams.Add("f", "json");
            reqParams.Add("token", token);

            string error = null;

            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, "admin/security/roles/getRolesForUser");
            msg.Content = new FormUrlEncodedContent(reqParams);
            msg.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            HttpResponseMessage response = await client.SendAsync(msg);

            if (!response.IsSuccessStatusCode)
            {
                return new List<string>();
            }
            try
            {
                response.EnsureSuccessStatusCode();

                string json = await response.Content.ReadAsStringAsync();

                RolesJson rolesJson = JsonConvert.DeserializeObject<RolesJson>(json);
                return rolesJson.Roles;
            }
            catch (HttpRequestException ex)
            {
                error = "Http Request Exception: " + ex.Message;
            }
            catch (JsonSerializationException ex)
            {
                error = "Json serialization exception: " + ex.Message;
            }
            catch (Exception ex)
            {
                error = "Exception: " + ex.Message;
            }
            return new List<string>();
        }

        public IGISConfig GetIgisConfig()
        {
            return _igisConfig;
        }

        private async Task<TokenResult> GetTokenFromArcgisPortal(string username, string password, int minutesExpire)
        {
            HttpClient client = CreateNewHttpClient(_igisConfig.PortalUrl);
            
            var reqParams = new Dictionary<string, string>();
            reqParams.Add("username", username);
            reqParams.Add("password", password);
            reqParams.Add("client", "requestip");
            reqParams.Add("expiration", minutesExpire.ToString());
            reqParams.Add("f", "json");
            HttpResponseMessage response;
            try
            {
                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, "sharing/rest/generateToken");
                msg.Content = new FormUrlEncodedContent(reqParams);
                msg.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                response = await client.SendAsync(msg);
            }
            catch (Exception ex)
            {
                return new TokenResult()
                {
                    Success = false,
                    ErrorReason = "Failed to connect to ArcGIS portal: " + ex.Message
                };
            }

            if (!response.IsSuccessStatusCode)
            {
                return new TokenResult()
                {
                    Success = false,
                    ErrorReason = "Failed to get token: Code: " + response.StatusCode + " : " + response.ReasonPhrase
                };
            }
            try
            {
                response.EnsureSuccessStatusCode();
                string json = await response.Content.ReadAsStringAsync();
                TokenJson token = JsonConvert.DeserializeObject<TokenJson>(json);
                return new TokenResult(token) { };
            }
            catch (HttpRequestException ex)
            {
                return new TokenResult() { Success = false, ErrorReason = "Http Request Exception: " + ex.Message };
            }
            catch (JsonSerializationException ex)
            {
                return new TokenResult() { Success = false, ErrorReason = "Json serialization exception: " + ex.Message };
            }
            catch (Exception ex)
            {
                return new TokenResult() { Success = false, ErrorReason = "Exception: " + ex.Message };
            }
        }
    }

}
