﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using IGISAccess.Models;
using Newtonsoft.Json;

namespace IGISWebApp.Services
{
    public static class IGISAdminAccess
    {
        public static async Task InsertIgisAccessInfoAsync(string insertIgisUrl, string username, string role)
        {
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

            HttpClient client = new HttpClient(handler)
            {
                BaseAddress = new Uri(insertIgisUrl)
            };

            try
            {
                string jsonOutput = JsonConvert.SerializeObject(new UserActivityInsertParam
                {
                    username = username,
                    role = role,
                    layer = "*IGIS Web App",
                    inserts = 0,
                    updates = 0,
                    deletes = 0,
                    date = DateTime.Today.Date
                });
                var response = await client.PostAsync("", new StringContent(jsonOutput));
                var message = response.EnsureSuccessStatusCode();

            }
            catch (Exception ex)
            {
                return;
            }



        }
    }
}
