﻿using IGISAccess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace IGISAccess.ArcgisAccess
{
    public class IGISConfigAccess
    {
        string filePath;

        public IGISConfigAccess()
        {
            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            var fileDir = new FileInfo(location.AbsolutePath).Directory.FullName;
            filePath = Path.Combine(fileDir, "IGISConfig.json");
        }

        public IGISConfig GetConfig()
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("The IGIS Config file is not found.", filePath);
            }

            IGISConfig config = null;
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                config = JsonConvert.DeserializeObject<IGISConfig>(json);
            }
            return config;
        }

        public IGISConfig GetConfigForDevelopment()
        {
            IGISConfig config = GetConfig();

            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            var fileDir = new FileInfo(location.AbsolutePath).Directory.FullName;
            string devFilePath = Path.Combine(fileDir, "IGISConfig.Development.json");

            if (File.Exists(devFilePath))
            {
                IGISConfig devConfig = null;
                using (StreamReader r = new StreamReader(devFilePath))
                {
                    string json = r.ReadToEnd();
                    devConfig = JsonConvert.DeserializeObject<IGISConfig>(json);
                }

                config.AdminConnectionString = devConfig.AdminConnectionString;
                config.InsertActivityUrl = devConfig.InsertActivityUrl;
                config.TokenExpiryMinutes = devConfig.TokenExpiryMinutes;
            }
            return config;
        }
    }
}
