﻿using IGISAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace IGISAccess.ArcgisAccess
{
    public interface IArcGISRoleManager
    {
        Privilege GetRolePrivilege(List<string> rolesToCheck, out string message);
        Privilege GetRolePrivilege(string role);
        string GetRole(List<string> rolesToCheck, out string message);
        List<Privilege> GetAllPrivileges(out string message, int appId);
        bool SetWebMapReference(int appid, int roleid, string webmap, string webmapref, out string message);
        List<LocalAuthority> GetLocalAuthorityMap(out string message);
    }

    public class ArcGISRoleManager : IArcGISRoleManager
    {
        string ConnectionString { get; set; }
        int appId;

        public ArcGISRoleManager(IGISConfig appConfig, int appId)
        {
            ConnectionString = appConfig.AdminConnectionString;
            this.appId = appId;
        }

        private List<Role> GetRoles(out string message)
        {
            message = "";
            List<Role> list = new List<Role>();

            SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
            try
            {
                using (igisAdminConnection)
                {
                    igisAdminConnection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT  RoleName FROM " +
                        IGISConstants.RoleMapTable, igisAdminConnection);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Role()
                            {
                                label = reader["RoleName"].ToString()
                            });
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                message = "SQL Exception @ " + System.Reflection.MethodBase.GetCurrentMethod() + " : " + ex.Message;
                return null;
            }
            return list;
        }

        private Role ConfirmRole(string role)
        {
            Role returnRole = null;

            SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
            using (igisAdminConnection)
            {
                igisAdminConnection.Open();
                SqlCommand cmd = new SqlCommand("SELECT  RoleName FROM " + IGISConstants.RoleMapTable +
                    " where RoleId = @role", igisAdminConnection);
                cmd.Parameters.AddWithValue("@role", role);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        returnRole = new Role()
                        {
                            label = reader["RoleName"].ToString()
                        };
                    }
                }
            }
            return returnRole;
        }

        private Privilege GetPrivilegeForRole(string roleToCheck, out string message, int appIdOverride = 0)
        {
            int findAppId = appIdOverride == 0 ? appId : appIdOverride;
            Privilege returnPrivilege = null;
            message = "";
            SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
            try
            {
                using (igisAdminConnection)
                {
                    igisAdminConnection.Open();
                    SqlCommand cmd = new SqlCommand(
                    "SELECT region.Name as Region, priv.RoleId as RoleId, RoleName" +
                        " FROM " + IGISConstants.RoleMapTable + " as priv " +
                        "left join " + IGISConstants.RegionMapTable + " as region on priv.RegionId = region.RegionId " +
                        "where priv.RoleName = @roleToCheck", igisAdminConnection);
                    cmd.Parameters.AddWithValue("@roleToCheck", roleToCheck);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            returnPrivilege = new Privilege();
                            returnPrivilege.RoleName = reader["RoleName"].ToString();
                            returnPrivilege.RoleId = (int)reader["RoleId"];
                            returnPrivilege.Region = reader["Region"].ToString();
                        }
                    }

                    cmd = new SqlCommand(
                        "SELECT webmap.WebMap,  webmap.WebMapRef" +
                        " FROM " + IGISConstants.RoleMapTable + " as priv " +
                        "left join " + IGISConstants.WebmapTable + " as webmap on priv.RoleId = webmap.RoleId " +
                        "where priv.RoleName = @roleToCheck and webmap.ApplicationId = @appId"
                        , igisAdminConnection);
                    cmd.Parameters.AddWithValue("@roleToCheck", roleToCheck);
                    cmd.Parameters.AddWithValue("@appId", findAppId.ToString());

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            returnPrivilege.WebMap = reader["WebMap"].ToString();
                            returnPrivilege.WebMapReference = reader["WebMapRef"].ToString();
                        }
                    }
                }
                return returnPrivilege;
            }
            catch (SqlException ex)
            {
                message = "SQL Exception @ " + System.Reflection.MethodBase.GetCurrentMethod() + " : " + ex.Message;
                return null;
            }
        }

        private List<string> GetLocalAuthorityForRegion(string region)
        {
            List<string> localAuthorities = new List<string>();

            SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
            using (igisAdminConnection)
            {
                igisAdminConnection.Open();
                SqlCommand cmd = new SqlCommand(
                    "SELECT LA.Name as laname FROM " + IGISConstants.LAtoRegionMapTable + " as la2region" +
                    " join " + IGISConstants.RegionMapTable + " as reg on reg.RegionId = la2region.RegionId " +
                    " join " + IGISConstants.LocalAuthorityTable + " as LA on LA.LocalAuthorityId = la2region.LocalAuthorityId " +
                    " where reg.Name = @region", igisAdminConnection);
                cmd.Parameters.AddWithValue("@region", region);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        localAuthorities.Add(reader["laname"].ToString());
                    }
                }
            }
            return localAuthorities;
        }

        private void SetPrivilegeAttributes(Privilege privilege, int appIdOverride = 0)
        {
            List<LayerPrivilege> privileges = new List<LayerPrivilege>();
            int findAppId = appIdOverride == 0 ? appId : appIdOverride;
            privilege.AppId = findAppId;
            try
            {

                SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
                using (igisAdminConnection)
                {
                    igisAdminConnection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT Name FROM " + IGISConstants.ApplicationTable +
                    " where ApplicationId = @appId", igisAdminConnection);
                    cmd.Parameters.AddWithValue("@appId", findAppId.ToString());

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            privilege.AppName = reader["Name"].ToString();
                        }
                    }
                }

                //Get layer privileges for role
                igisAdminConnection = new SqlConnection(ConnectionString);
                using (igisAdminConnection)
                {
                    igisAdminConnection.Open();

                    foreach ((string TableName, LayerPrivilege LayerPrivilege) tableToLayerPriv
                        in IGISConstants.TableToLayerPrivileges)
                    {
                        SqlCommand cmd = new SqlCommand("SELECT CanEdit, CanView  FROM " + tableToLayerPriv.TableName +
                            " where ApplicationId = @appId and RoleId = " + privilege.RoleId.ToString()
                            , igisAdminConnection);
                        cmd.Parameters.AddWithValue("@appId", findAppId.ToString());

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                LayerPrivilege layerPrivilege = new LayerPrivilege() { };
                                layerPrivilege.Layer = tableToLayerPriv.LayerPrivilege.Layer;
                                layerPrivilege.CanEdit = Boolean.Parse(reader["CanEdit"].ToString());
                                layerPrivilege.CanView = Boolean.Parse(reader["CanView"].ToString());
                                privileges.Add(layerPrivilege);
                            }
                        }
                    }
                }
                privilege.Layers = privileges;
                privilege.LocalAuthorities = GetLocalAuthorityForRegion(privilege.Region);
            }
            catch (SqlException)
            { }//do nothing 
        }

        public Privilege GetRolePrivilege(string role)
        {
            string message;
            Privilege returnPrivilege = GetPrivilegeForRole(role, out message);
            if (!String.IsNullOrEmpty(message))
            {
                return returnPrivilege;
            }
            SetPrivilegeAttributes(returnPrivilege);
            return returnPrivilege;
        }

        public string GetRole(List<string> rolesToCheck, out string message)
        {
            message = "";
            string returnRole = null;
            List<Role> roles = GetRoles(out message);

            if (!String.IsNullOrEmpty(message))
            {
                return null;
            }

            if (!roles.Select(role => role.RoleName).Intersect(rolesToCheck).Any())
            {
                message = "Roles returned not matched to any in configuration. Please check roles set in arcgis server with those in admin database role table.";
                return returnRole;
            }
            List<string> roleNames = roles.Select(role => role.RoleName).ToList<string>();
            foreach (string roleCheck in roleNames)
            {
                if (roleNames.Contains(roleCheck))
                {
                    return roleCheck;
                }
            }
            return returnRole;
        }

        public Privilege GetRolePrivilege(List<string> rolesToCheck, out string message)
        {
            message = "";
            Privilege returnPrivilege = new Privilege();
            List<Role> roles = GetRoles(out message);
            if (!String.IsNullOrEmpty(message))
            {
                return returnPrivilege;
            }
            if (!roles.Select(role => role.RoleName).Intersect(rolesToCheck).Any())
            {
                message = "Roles returned not matched to any in configuration. Please check roles set in arcgis server with those in admin database role table.";
                return returnPrivilege;
            }

            Privilege chosenPrivilege = null;
            foreach (string role in rolesToCheck)
            {
                chosenPrivilege = GetPrivilegeForRole(role, out message);
                if (!String.IsNullOrEmpty(message))
                {
                    return returnPrivilege;
                }
                if (chosenPrivilege != null)
                    break;
            }
            if (chosenPrivilege == null)
            {
                message = "Cannot find role configured.";
                return returnPrivilege;
            }

            if (String.IsNullOrWhiteSpace(chosenPrivilege.WebMapReference))
            {
                message = "Web map not assigned for this user";
                return returnPrivilege;
            }
            SetPrivilegeAttributes(chosenPrivilege);

            return chosenPrivilege;
        }

        public List<Privilege> GetAllPrivileges(out string message, int appId)
        {
            List<Privilege> returnPrivileges = new List<Privilege>();
            List<Role> roles = GetRoles(out message);
            if (!String.IsNullOrEmpty(message))
            {
                return returnPrivileges;
            }
            foreach (Role role in roles)
            {
                Privilege priv = GetPrivilegeForRole(role.RoleName, out message, appId);
                if (!String.IsNullOrEmpty(message))
                {
                    return returnPrivileges;
                }
                SetPrivilegeAttributes(priv, appId);
                returnPrivileges.Add(priv);
            }
            return returnPrivileges;
        }

        public bool SetWebMapReference(int appid, int roleid, string webmap, string webmapref, out string message)
        {
            message = "";
            SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
            try
            {
                using (igisAdminConnection)
                {
                    igisAdminConnection.Open();
                    SqlCommand cmd = new SqlCommand(
                    "SELECT webmap.WebMap , webmap.WebMapRef FROM " + IGISConstants.WebmapTable + " as webmap " +
                        "where webmap.RoleId = @roleId AND webmap.ApplicationId = @appId ", igisAdminConnection);
                    cmd.Parameters.AddWithValue("@roleId", roleid.ToString());
                    cmd.Parameters.AddWithValue("@appId", appid.ToString());

                    List<(string webmap, string webmapref)> existingWebMaps = new List<(string webmap, string webmapref)>();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            existingWebMaps.Add((webmap: reader["WebMap"].ToString(),
                                webmapref: reader["WebMapRef"].ToString()));
                        }
                    }
                    if (existingWebMaps.Count > 0)
                    {//then do update on first one
                        if (existingWebMaps[0].webmapref != webmapref && !String.IsNullOrEmpty(existingWebMaps[0].webmap))
                        {
                            cmd = new SqlCommand("UPDATE " + IGISConstants.WebmapTable + " SET WebMapRef = @WebMapRef " +
                                "where webmap.RoleId = @roleId AND webmap.ApplicationId = @appId;", igisAdminConnection);
                            cmd.Parameters.AddWithValue("@roleId", roleid.ToString());
                            cmd.Parameters.AddWithValue("@appId", appid.ToString());
                            cmd.Parameters.AddWithValue("@WebMapRef", webmapref.ToString());
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {//then insert new line
                        cmd = new SqlCommand(String.Format("SET IDENTITY_INSERT {0} ON; INSERT INTO {0} " +
                            "(ApplicationId, RoleId, WebMap, WebMapRef) " +
                            " VALUES(@appId, @roleId, @webmap, @webmapref);", IGISConstants.WebmapTable),
                            igisAdminConnection);
                        cmd.Parameters.AddWithValue("@roleId", roleid.ToString());
                        cmd.Parameters.AddWithValue("@appId", appid.ToString());
                        cmd.Parameters.AddWithValue("@webmapref", webmapref);
                        cmd.Parameters.AddWithValue("@webmap", webmap);
                        cmd.ExecuteNonQuery();
                    }
                    igisAdminConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        public List<LocalAuthority> GetLocalAuthorityMap(out string message)
        {
            message = "";
            List<LocalAuthority> localAuthorities = new List<LocalAuthority>();
            try
            {
                SqlConnection igisAdminConnection = new SqlConnection(ConnectionString);
                using (igisAdminConnection)
                {
                    igisAdminConnection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT Name, Code FROM " + IGISConstants.LocalAuthorityTable, igisAdminConnection);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LocalAuthority la = new LocalAuthority()
                            {
                                Name = reader["Name"].ToString(),
                                Code = reader["Code"].ToString()
                            };
                            localAuthorities.Add(la);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                message = ex.Message;
            }

            return localAuthorities;
        }
    }
}
