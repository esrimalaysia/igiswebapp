﻿using IGISAccess.Models;
using IGISAccess.ViewModels;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace IGISAccess.Services
{
    public interface ICemsService
    {
        List<CemsViewModel> GetCems();
    }

    public class CemsService : ICemsService
    {
        string ConnectionString { get; set; }
        string CemsTable { get; set; }

        public CemsService(IGISConfig appConfig)
        {
            ConnectionString = appConfig.IgisConnectionString;
            CemsTable = IGISConstants.CEMSTable;
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        public List<CemsViewModel> GetCems()
        {
            List<CemsViewModel> list = new List<CemsViewModel>();

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select * from " + CemsTable, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new CemsViewModel()
                        {
                            STPCD = reader["STPCD"].ToString(),
                            RESOLVED = reader["RESOLVED"].ToString(),
                            UNRESOLVED = reader["UNRESOLVED"].ToString(),
                            TEMP_RESOLVED = reader["TEMP_RESOLVED"].ToString(),
                            ENQTYPECD = reader["ENQTYPECD"].ToString(),
                            ENQTYPE_DESC = reader["ENQTYPE_DESC"].ToString(),

                        });
                    }
                }
            }

            return list;
        }
    }
}

