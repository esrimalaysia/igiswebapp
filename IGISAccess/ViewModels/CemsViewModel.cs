﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace IGISAccess.ViewModels
{
    public class CemsViewModel
    {
        public int IGISCEMSID { get; set; }

        public string STPCD { get; set; }

        public string RESOLVED { get; set; }

        public string UNRESOLVED { get; set; }

        public string TEMP_RESOLVED { get; set; }

        public string ENQTYPECD { get; set; }

        public string ENQTYPE_DESC { get; set; }
    }
}
