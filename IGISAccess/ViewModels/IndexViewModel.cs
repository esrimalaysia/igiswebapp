﻿using IGISAccess.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGISAccess.ViewModels
{
    [Serializable]
    public class IndexViewModel
    {
        string password;
        public string Username { get; set; }
        public string WebMapReference { get; set; }
        public string Token { get; set; }
        public string PortalToken { get; set; }
        public string RoleName { get; set; }
        public string Region { get; set; }
        public long Expires { get; set; }
        public string UserActivityUrl { get; set; }
        public string ArcgisServerUrl { get; set; }
        public string ArcgisPortalUrl { get; set; }
        public string LAQueryUrl { get; set; }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = SecureStringHelper.EncryptText(value);
            }
        }
        public string LocalAuthorities { get; set; }
        public string ManholeQueryUrl { get; set; }
        public string SewerlineQueryUrl1 { get; set; }
        public string SewerlineQueryUrl2 { get; set; }
        public string UpdateCEMSUrl { get; set; }
        public string RestComplaintUrl { get; set; }
    }
}
