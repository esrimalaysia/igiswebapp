///////////////////////////////////////////////////////////////////////////
// Copyright © Esri. All Rights Reserved.
//
// Licensed under the Apache License Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////

define([
    './ConfigManager',
    './LayoutManager',
    './DataManager',
    './WidgetManager',
    './FeatureActionManager',
    './SelectionManager',
    './DataSourceManager',
    './FilterManager',
    'dojo/_base/html',
    'dojo/_base/lang',
    'dojo/_base/array',
    'dojo/on',
    'dojo/keys',
	'dojo/mouse',
    'dojo/topic',
    'dojo/cookie',
    'dojo/Deferred',
    'dojo/promise/all',
    'dojo/io-query',
    'esri/config',
    'esri/request',
    'esri/urlUtils',
    'esri/IdentityManager',
    'jimu/portalUrlUtils',
    './utils',
    'require',
    'dojo/i18n',
    'dojo/i18n!./nls/main',
    'esri/main',
    'dojo/ready',
    'dojo'
  ],
  function(ConfigManager, LayoutManager, DataManager, WidgetManager, FeatureActionManager, SelectionManager,
    DataSourceManager, FilterManager, html, lang, array, on, keys, mouse,
    topic, cookie, Deferred, all, ioquery, esriConfig, esriRequest, urlUitls, IdentityManager,
    portalUrlUtils, jimuUtils, require, i18n, mainBundle, esriMain, dojoReady, dojo) {
    /* global jimuConfig:true */
    var mo = {}, appConfig;

    window.topic = topic;

    //set the default timeout to 3 minutes
    esriConfig.defaults.io.timeout = 60000 * 3;

    //patch for JS API 3.10
    var hasMethod = typeof cookie.getAll === 'function';
    if (!hasMethod) {
      cookie.getAll = function(e) {
        var result = [];
        var v = cookie(e);
        if (v) {
          result.push(v);
        }
        return result;
      };
    }

    //jimu nls
    window.jimuNls = mainBundle;
    window.apiNls = esriMain.bundle;

    IdentityManager.setProtocolErrorHandler(function() {
      return true;
    });

      //EsriM mod: get token when 
      setIdentityManager = function (token, portaltoken, expire, username) {// Register token from arcgis server with the IdentityManager
          let arcgisSvrUrl = sessionStorage.getItem("igisArcgisSvrUrl");
          let arcgisPortalUrl = sessionStorage.getItem("igisPortalUrl");
          if (token) {
              idObject = {
                  "serverInfos": [{
                      "server": arcgisSvrUrl,
                      "tokenServiceUrl": arcgisSvrUrl + "/tokens/",
                      "adminTokenServiceUrl": arcgisSvrUrl + "/admin/generateToken",
                      "shortLivedTokenValidity": 480,
                      "currentVersion": '10.7.1',
                      "hasServer": true
                  }],
                  "credentials": [{
                      "userId": username,
                      "server": arcgisSvrUrl,
                      "token": token,
                      "expires": expire,
                      "validity": 720,
                      "ssl": false,
                      "creationTime": Date.now(),
                      "scope": "server"
                  }]
              };
              IdentityManager.initialize(idObject);
              IdentityManager.tokenValidity = 720;
          }
          if (portaltoken) {
              //IdentityManager.registerToken({
              //    server: arcgisPortalUrl + "/sharing/rest/",
              //    token: portaltoken
              //});

              let serverInfo = {
                  server: "https://igis.iwk.com.my/geoportal",
                  tokenServiceUrl: "https://igis.iwk.com.my/geoportal/sharing/rest/generateToken/",
                  hasPortal: true,
                  webTierAuth: false
              };

              let credential = {
                  userId: "portaladmin",
                  server: "https://igis.iwk.com.my/geoportal",
                  token: portaltoken,
                  //expires: expire,
                  ssl: false,
                  scope: "portal" 
              };

              var idObject = {
                  serverInfos: [serverInfo],
                  credentials: [credential]
              };

              IdentityManager.initialize(idObject);
          }
      };

    refreshToken = function () {
        let username = sessionStorage.getItem("igiswebappuser");    
        let igispw = sessionStorage.getItem("igiswebappw");

        fetch(window.location.href + "account/gettoken", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify({ Username: username, Password: igispw }) // body data type must match "Content-Type" header
        })
        .then(async function (response) {
            if (response.ok) {
                const result = await response.json();
                console.log('Success:', JSON.stringify(result));
                expiryTimeTicks = result.expireTicks;
                igistoken = result.token;
                setIdentityManager(result.token, result.portaltoken, expiryTimeTicks, sessionStorage.getItem("igiswebappuser"));
                showResetting = false;
            }
            else {
                throw new Error(response.statusText);
            }
        }.bind(this)
        ).catch(function (error) {
            // If there is any error you will catch them here
            console.log(error.message);
        });          
    };

      function displayTime(ticks) {
          let seconds = ticks / 1000;
          let hh = Math.floor(seconds / 3600);
          let mm = Math.floor((seconds - hh * 3600) / 60);
          let ss = Math.floor(seconds % 60);

          return (pad(hh, 2) + ":" + pad(mm, 2) + ":" + pad(ss, 2));
      }

      function pad(n, width) {
          n = n + '';
          return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
      }

      function startTimer() {
          function timer() {
              if (showResetting) {
                  document.getElementById("timeRemain").innerHTML = "Resetting";
              }
              else {
                  timeDiff = expiryTimeTicks - Date.now();

                  if (timeDiff <= 0) {
                      window.location.href = window.location.href + "account/logout";
                      alert("Session has ended, please login again.");
                      clearInterval(remainingIntervalId);
                  }
                  document.getElementById("timeRemain").innerHTML = displayTime(timeDiff);
              }
          };
          let remainingIntervalId = setInterval(timer, 1000);
      }

      let expiryTime = sessionStorage.getItem("igiswebappexpire"); 
      var expiryTimeTicks = parseInt(expiryTime, 10);
      var igistoken = sessionStorage.getItem("igistoken");
      var igisPortalToken = sessionStorage.getItem("igisportaltoken");
      var showResetting = false;
      setIdentityManager(igistoken, igisPortalToken, expiryTimeTicks, sessionStorage.getItem("igiswebappuser"));
      startTimer();
      document.getElementById("timeRemain").innerHTML = "Start";

      var igisTokenResetBtn = dojo.byId("igisTokenResetBtn");
      on(igisTokenResetBtn, "click", function (evt) {
          refreshToken();
          showResetting = true;
      });
    //End esriM mod

    var ancestorWindow = jimuUtils.getAncestorWindow();
    var parentHttps = false, patt = /^http(s?):\/\//gi;

    try {
      parentHttps = ancestorWindow.location.href.indexOf("https://") === 0;
    } catch (err) {
      //if it's in different domain, we do not force https

      // console.log(err);
      // parentHttps = window.location.protocol === "https:";
    }

    esriRequest.setRequestPreCallback(function(ioArgs) {
      if (ioArgs.content && ioArgs.content.printFlag) { // printTask
        ioArgs.timeout = 300000;
      }

      //use https protocol
      if (parentHttps) {
	 
        ioArgs.url = ioArgs.url.replace(patt, '//');
      }

      //working around an arcgis server feature service bug.
      //Requests to queryRelatedRecords operation fail with feature service 10.
      //Detect if request conatins the queryRelatedRecords operation
      //and then change the source url for that request to the corresponding mapservice.
      if (ioArgs.url.indexOf("/queryRelatedRecords?") !== -1) {
        var serviceUrl = ioArgs.url;
        var proxyUrl = esriConfig.defaults.io.proxyUrl;
        if(proxyUrl && ioArgs.url.indexOf(proxyUrl + "?") === 0){
          //This request uses proxy.
          //We should remove proxyUrl to get the real service url to detect if it is a hosted service or not.
          serviceUrl = ioArgs.url.replace(proxyUrl + "?", "");
        }
        if (!jimuUtils.isHostedService(serviceUrl)) { // hosted service doesn't depend on MapServer
          ioArgs.url = ioArgs.url.replace("FeatureServer", "MapServer");
        }
      }

      //For getJobStatus of gp service running in safari.
      //The url of requests sent to getJobStatus is the same. In safari, the requests will be blocked except
      //the first one. Here a preventCache tag is added for this kind of request.
      var reg = /GPServer\/.+\/jobs/;
      if (reg.test(ioArgs.url)) {
        ioArgs.preventCache = new Date().getTime();
      }

      // Use proxies to replace the premium content
      if(!window.isBuilder && appConfig && !appConfig.mode) {
        if (appConfig.appProxies && appConfig.appProxies.length > 0) {
		   
          array.some(appConfig.appProxies, function(proxyItem) {
            var sourceUrl = proxyItem.sourceUrl, proxyUrl = proxyItem.proxyUrl;
            if (parentHttps) {
              sourceUrl = sourceUrl.replace(patt, '//');
              proxyUrl = proxyUrl.replace(patt, '//');
            }
            if(ioArgs.url.indexOf(sourceUrl) >= 0) {
              ioArgs.url = ioArgs.url.replace(sourceUrl, proxyUrl);
              return true;
            }
          });
        }
        if (appConfig.map.appProxy) {
          array.some(appConfig.map.appProxy.proxyItems, function(proxyItem) {
            if (!proxyItem.useProxy || !proxyItem.proxyUrl) {
              return false;
            }
            var sourceUrl = proxyItem.sourceUrl, proxyUrl = proxyItem.proxyUrl;
            if (parentHttps) {
              sourceUrl = sourceUrl.replace(patt, '//');
              proxyUrl = proxyUrl.replace(patt, '//');
            }
            if (ioArgs.url.indexOf(sourceUrl) >= 0) {
              ioArgs.url = ioArgs.url.replace(sourceUrl, proxyUrl);
              return true;
            }
          });
        }
      }

      return ioArgs;
    });


    // disable middle mouse button scroll
    on(window, 'mousedown', function(evt) {
      if(jimuUtils.isInNavMode()){
        html.removeClass(document.body, 'jimu-nav-mode');
        window.isMoveFocusWhenInit = false;
      }
      if (!mouse.isMiddle(evt)) {
        return;
      }

      evt.preventDefault();
      evt.stopPropagation();
      evt.returnValue = false;
      return false;
    });
    on(window, 'keydown', function(evt) {
      if(evt.keyCode === keys.TAB && !jimuUtils.isInNavMode()){
        html.addClass(document.body, 'jimu-nav-mode');
      }
    });

    String.prototype.startWith = function(str) {
      if (this.substr(0, str.length) === str) {
        return true;
      } else {
        return false;
      }
    };

    String.prototype.endWith = function(str) {
      if (this.substr(this.length - str.length, str.length) === str) {
        return true;
      } else {
        return false;
      }
    };

    // Polyfill isNaN for IE11
    // Source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
    Number.isNaN = Number.isNaN || function (value) {
      return value !== value;
    };

    /*jshint unused: false*/
    if (typeof jimuConfig === 'undefined') {
      jimuConfig = {};
    }
    jimuConfig = lang.mixin({
      loadingId: 'main-loading',
      loadingImageId: 'app-loading',
      loadingGifId: 'loading-gif',
      layoutId: 'jimu-layout-manager',
      mapId: 'map',
      mainPageId: 'main-page',
      timeout: 5000,
      breakPoints: [600, 1280]
    }, jimuConfig);


    window.wabVersion = '2.13';
    // window.productVersion = 'Online 7.2';
    window.productVersion = 'Web AppBuilder for ArcGIS (Developer Edition) 2.13';
    // window.productVersion = 'Portal for ArcGIS 10.7';

    function initApp() {
      var urlParams, configManager, layoutManager;
      console.log('jimu.js init...');
      urlParams = getUrlParams();

      if(urlParams.mobileBreakPoint){
        try{
          var bp = parseInt(urlParams.mobileBreakPoint, 10);
          jimuConfig.breakPoints[0] = bp;
        }catch(err){
          console.error('mobileBreakPoint URL parameter must be a number.', err);
        }
      }

      if(urlParams.mode){
        html.setStyle(jimuConfig.loadingId, 'display', 'none');
        html.setStyle(jimuConfig.mainPageId, 'display', 'block');
      }
      //the order of initialize these managers does mater because this will affect the order of event listener.
      DataManager.getInstance(WidgetManager.getInstance());
      FeatureActionManager.getInstance();
      SelectionManager.getInstance();
      DataSourceManager.getInstance();
      FilterManager.getInstance();

      layoutManager = LayoutManager.getInstance({
        mapId: jimuConfig.mapId,
        urlParams: urlParams
      }, jimuConfig.layoutId);
      configManager = ConfigManager.getInstance(urlParams);

      layoutManager.startup();
      configManager.loadConfig();
      //load this module here to make load modules and load app parallelly
      require(['dynamic-modules/preload']);

      //temp fix for this issue: https://devtopia.esri.com/WebGIS/arcgis-webappbuilder/issues/14082
      dojoReady(function(){
        setTimeout(function(){
          html.removeClass(document.body, 'dj_a11y');
        }, 50);
      });
    }

    function getUrlParams() {
      var s = window.location.search,
        p;
      // params that don't need to `sanitizeHTML`
      var exceptUrlParams = {
        query: true
      };
      if (s === '') {
        return {};
      }

      p = ioquery.queryToObject(s.substr(1));

      for(var k in p){
        if(!exceptUrlParams[k]){
          p[k] = jimuUtils.sanitizeHTML(p[k]);
        }
      }
      return p;
    }

    if(window.isBuilder){
      topic.subscribe("app/appConfigLoaded", onAppConfigChanged);
      topic.subscribe("app/appConfigChanged", onAppConfigChanged);
    }else{
      topic.subscribe("appConfigLoaded", onAppConfigChanged);
      topic.subscribe("appConfigChanged", onAppConfigChanged);
    }

    function onAppConfigChanged(_appConfig, reason){
      appConfig = _appConfig;

      if(reason === 'loadingPageChange'){
        return;
      }

      html.setStyle(jimuConfig.mainPageId, 'display', 'block');
    }
    //ie css
    var ieVersion = jimuUtils.has('ie');
    if(ieVersion){
      if(ieVersion > 9){
        html.addClass(document.body, 'ie-nav-mode');
      }else{
        html.addClass(document.body, 'ie-low-nav-mode');
      }
      if(ieVersion > 10){
        html.addClass(document.body, 'ie-gte-10');
      }
    }
    mo.initApp = initApp;
    return mo;
  });