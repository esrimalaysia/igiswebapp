﻿using IGISAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IGISAccess
{
    public static class IGISConstants
    {
        //Table names
        public const string CEMSTable = "COEDS";
        public const string UseActivityTable = "User_Activity";
        public const string RoleMapTable = "Role";
        public const string RegionMapTable = "Region";
        public const string WebmapTable = "WebMap";
        public const string ApplicationTable = "Application";
        public const string LAtoRegionMapTable = "LAtoRegionMap";
        public const string LocalAuthorityTable = "LocalAuthority";

        //Layer Privileges
        public const string NewDevLayerPrivilegeTable = "NewDevelopmentPrivileges";
        public const string OpsLineLayerPrivilegeTable = "OperationLinePrivileges";
        public const string OpsPointLayerPrivilegeTable = "OperationPointPrivileges";
        public const string RiskSewerlineLayerPrivilegeTable = "RiskSewerlinePrivileges";
        public static List<(string, LayerPrivilege)> TableToLayerPrivileges =
            new List<(string TableName, LayerPrivilege LayerPrivilege)>()
        {
            (NewDevLayerPrivilegeTable, new LayerPrivilege(){ Layer= "New Development" }),
            (OpsLineLayerPrivilegeTable, new LayerPrivilege(){ Layer= "Operation (Line)" }),
            (OpsPointLayerPrivilegeTable, new LayerPrivilege(){ Layer= "Operation (Point)" }),
            (RiskSewerlineLayerPrivilegeTable, new LayerPrivilege(){ Layer= "Risk Sewerline" })
        };

        public const string JWTKey = "mkey For igisadmin 1n iwk";
    }
}
