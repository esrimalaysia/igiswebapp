﻿using IGISAccess.Models;
using IGISAccess.Services;
using IGISAccess.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace IGISWebApp.Controllers
{
    public class DialogController : Controller
    {
        private readonly IGISConfig _appConfig;
        private ICemsService _cemsService;

        public DialogController(IGISConfig appConfig, ICemsService cemsService)
        {
            _appConfig = appConfig;
            _cemsService = cemsService;
        }

        [ActionName("attributedialog")]
        [Route("[action]")]
        [Route("[controller]/[action]/{site}/{param}")]
        public IActionResult AttributeDialog(string site = null, string param = null)
        {
            if (param == null || String.IsNullOrEmpty(param))
            {
                throw new ArgumentException("No 'param' parameter to search");
            }
            else if (site == null || String.IsNullOrEmpty(site))
            {
                throw new ArgumentException("No 'site' parameter to search");
            }
            else if (site == "Edms")
            {
                ViewData["QueryLink"] = _appConfig.EdmsQueryLink;
            }
            else if (site == "BrainsByAssetNo")
            {
                ViewData["QueryLink"] = _appConfig.BrainsAssetNoQueryLink;
            }
            else if (site == "BrainsBySan")
            {
                ViewData["QueryLink"] = _appConfig.BrainsSanQueryLink;
            }
            else if (site == "Cems")
            {
                ViewData["assetNo"] = param; 

                List<CemsViewModel> result = _cemsService.GetCems();
                var codesList = result.Where(x => x.STPCD.Equals(param));

                return View("Cems", codesList.ToList());
            }
            else
            {
                throw new ArgumentException("Incorrect 'site' parameter");
            }

            ViewData["Param"] = param;
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
