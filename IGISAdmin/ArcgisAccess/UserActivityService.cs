﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using IGISAccess;
using IGISAccess.Models;
using IGISAdmin.Models;

namespace IGISAdmin.ArcgisAccess
{
    public interface IUserActivityService
    {
        List<Role> GetRoles();
        List<Layer> GetLayers();
        List<UserActivityByRole> GetUserActivityByRole(DateTime startDate, DateTime endDate, string role, string layer);
        List<ActivityBreakdown> GetUserActivityByLayer(DateTime startDate, DateTime endDate, string role, string layer);
        List<ActivityBreakdown> GetLayerBreakdownByRole(DateTime startDate, DateTime endDate, string role, string layer);
        List<ActivityBreakdown> GetRoleBreakdownByLayer(DateTime startDate, DateTime endDate, string layer);
        int InsertUserActivityRecord(string username, string role, string layer, DateTime date, int inserts, int updates, int deletes);
        List<ActivityBreakdown> GetLayerBreakdownByUser(DateTime startDate, DateTime endDate, string user);
    }

    public class UserActivityService : IUserActivityService
    {
        string ConnectionString { get; set; }
        string UserActivityTable { get; set; }

        public UserActivityService(IGISConfig appConfig)
        {
            ConnectionString = appConfig.AdminConnectionString;
            UserActivityTable = IGISConstants.UseActivityTable;
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        public List<Role> GetRoles()
        {
            List<Role> list = new List<Role>();

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select distinct role from " + UserActivityTable, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Role()
                        {
                            label = reader["role"].ToString(),
                        });
                    }
                }
            }

            return list;
        }

        public List<Layer> GetLayers()
        {
            List<Layer> list = new List<Layer>();

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select distinct layer from " + UserActivityTable, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Layer()
                        {
                            label = reader["layer"].ToString(),
                        });
                    }
                }
            }

            return list;
        }

        public List<UserActivityByRole> GetUserActivityByRole(DateTime startDate, DateTime endDate, string role, string layer)
        {
            checkArgument(startDate != DateTime.MinValue, "start cannot be empty");
            checkArgument(endDate != DateTime.MinValue, "end cannot be empty");

            List<UserActivityByRole> list = new List<UserActivityByRole>();

            var isAllLayers = String.IsNullOrWhiteSpace(layer);
            var isAllRoles = String.IsNullOrWhiteSpace(role);

            // if is all roles, return list of roles, if specific role, return list of users, for specific role, getting all users
            string roleOrUser = isAllRoles ? "role" : "username"; //column names in sql
            
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(String.Format("select {0}, ", roleOrUser)
                        + "count(distinct date) as accessdays"
                        + (isAllLayers ? "" : ", sum(inserts) as inserts, sum(updates) as updates, sum(deletes) as deletes")
                        + " from " + UserActivityTable + " "
                        + "where date between @start and @end"
                        + (isAllRoles ? "" : " and role = @role")
                        + (isAllLayers ? "" : " and layer = @layer")
                        + String.Format(" group by {0}", roleOrUser)
                        , conn);

                cmd.Parameters.AddWithValue("@start", startDate);
                cmd.Parameters.AddWithValue("@end", endDate);
                
                if (!isAllRoles)
                {
                    cmd.Parameters.AddWithValue("@role", role);
                }

                if (!isAllLayers)
                {
                    cmd.Parameters.AddWithValue("@layer", layer);
                }

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var record = new UserActivityByRole()
                        {
                            label = reader[roleOrUser].ToString(),
                            accessDays = Convert.ToInt32(reader["accessdays"]),
                        };

                        if (!isAllLayers)
                        { //meant for specific layers query
                            record.insertCounts = Convert.ToInt32(reader["inserts"]);
                            record.updateCounts = Convert.ToInt32(reader["updates"]);
                            record.deleteCounts = Convert.ToInt32(reader["deletes"]);
                        }

                        list.Add(record);
                    }
                }
            }
            return list;
        }

        public List<ActivityBreakdown> GetUserActivityByLayer(DateTime startDate, DateTime endDate, string role, string layer)
        {
            checkArgument(startDate != DateTime.MinValue, "start cannot be empty");
            checkArgument(endDate != DateTime.MinValue, "end cannot be empty");

            List<ActivityBreakdown> list = new List<ActivityBreakdown>();
            bool filterByRole = !String.IsNullOrWhiteSpace(role);
            bool filterByLayer = !String.IsNullOrWhiteSpace(layer);

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select layer, sum(inserts) as inserts, sum(updates) as updates, sum(deletes) as deletes " 
                    + "from " + UserActivityTable + " "
                    + "where date between @start and @end"
                    + (filterByRole ? " and role = @role" : "")
                    + (filterByLayer ? " and layer = @layer" : "")
                    + " group by layer", conn);
                cmd.Parameters.AddWithValue("@start", startDate);
                cmd.Parameters.AddWithValue("@end", endDate);

                if (filterByRole)
                {
                    cmd.Parameters.AddWithValue("@role", role);
                }
                if (filterByLayer)
                {
                    cmd.Parameters.AddWithValue("@Layer", layer);
                }
                list = ReadSqlResponseOnColumn(cmd, "layer");
            }
            return list;
        }

        private List<ActivityBreakdown> ReadSqlResponseOnColumn(SqlCommand sqlCommand, string column)
        {
            List<ActivityBreakdown> list = new List<ActivityBreakdown>();
            using (var reader = sqlCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    list.Add(new ActivityBreakdown()
                    {
                        label = reader[column].ToString(),
                        insertCounts = Convert.ToInt32(reader["inserts"]),
                        updateCounts = Convert.ToInt32(reader["updates"]),
                        deleteCounts = Convert.ToInt32(reader["deletes"])
                    });
                }
            }
            return list;
        }

        public List<ActivityBreakdown> GetLayerBreakdownByRole(DateTime startDate, DateTime endDate, string role, string layer)
        {
            checkArgument(startDate != DateTime.MinValue, "start cannot be empty");
            checkArgument(endDate != DateTime.MinValue, "end cannot be empty");
            // TODO: role and layer can be null?

            List<ActivityBreakdown> list = new List<ActivityBreakdown>(); ;
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select layer, sum(inserts) as inserts, sum(updates) as updates, sum(deletes) as deletes "
                    + "from "+ UserActivityTable + " " 
                    + "where date between @start and @end and role = @role group by layer", conn);
                cmd.Parameters.AddWithValue("@start", startDate);
                cmd.Parameters.AddWithValue("@end", endDate);
                cmd.Parameters.AddWithValue("@role", role);
                                
                list = ReadSqlResponseOnColumn(cmd, "layer");
            }
            return list;
        }

        public List<ActivityBreakdown> GetLayerBreakdownByUser(DateTime startDate, DateTime endDate, string user)
        {
            checkArgument(startDate != DateTime.MinValue, "start cannot be empty");
            checkArgument(endDate != DateTime.MinValue, "end cannot be empty");

            List<ActivityBreakdown> list = new List<ActivityBreakdown>(); ;
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select layer, sum(inserts) as inserts, sum(updates) as updates, sum(deletes) as deletes "
                    + "from " + UserActivityTable + " "
                    + "where date between @start and @end and username = @user group by layer", conn);
                cmd.Parameters.AddWithValue("@start", startDate);
                cmd.Parameters.AddWithValue("@end", endDate);
                cmd.Parameters.AddWithValue("@user", user);

                list = ReadSqlResponseOnColumn(cmd, "layer");
            }
            return list;
        }

        public List<ActivityBreakdown> GetRoleBreakdownByLayer(DateTime startDate, DateTime endDate, string layer)
        {
            checkArgument(startDate != DateTime.MinValue, "start cannot be empty");
            checkArgument(endDate != DateTime.MinValue, "end cannot be empty");
            checkArgument(!String.IsNullOrWhiteSpace(layer), "layer cannot be empty");

            List<ActivityBreakdown> list = new List<ActivityBreakdown>();

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select role, sum(inserts) as inserts, sum(updates) as updates, sum(deletes) as deletes "
                    + "from " + UserActivityTable + " " 
                    + "where date between @start and @end and layer = @layer group by role", conn);
                cmd.Parameters.AddWithValue("@start", startDate);
                cmd.Parameters.AddWithValue("@end", endDate);
                cmd.Parameters.AddWithValue("@layer", layer);

                list = ReadSqlResponseOnColumn(cmd, "role");
            }
            return list;
        }

        public int InsertUserActivityRecord(string username, string role, string layer, DateTime date, int inserts, int updates, int deletes)
        {
            checkArgument(date != DateTime.MinValue, "date cannot be empty");
            checkArgument(!String.IsNullOrWhiteSpace(role), "role cannot be empty");
            checkArgument(!String.IsNullOrWhiteSpace(layer), "layer cannot be empty");
            checkArgument(!String.IsNullOrWhiteSpace(username), "username cannot be empty");
            checkArgument(inserts >= 0, "inserts cannot be negative number");
            checkArgument(updates >= 0, "updates cannot be negative number");
            checkArgument(deletes >= 0, "deletes cannot be negative number");

            var dateInvariantSQLString = getInvariantSQLDate(date);
            var result = 0;

            using (SqlConnection conn = GetConnection())
            {
                var query = String.Format("MERGE INTO {0} WITH (SERIALIZABLE) ua", UserActivityTable)
                            + " USING (VALUES(@username, @role, @layer, @date)) AS checkUA (username, role, layer, date)"
                            + " ON(ua.username = checkUA.username AND ua.role = checkUA.role AND ua.layer = checkUA.layer AND ua.date = checkUA.date)"
                            + " WHEN MATCHED THEN"
                                + " UPDATE SET ua.inserts = ua.inserts + @inserts, ua.updates = ua.updates + @updates, ua.deletes = ua.deletes + @deletes"
                            + " WHEN NOT MATCHED THEN"
                                + " Insert (username, role, layer, date, inserts, updates, deletes)"
                                + " VALUES (@username, @role, @layer, @date, @inserts, @updates, @deletes);";

                conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@role", role);
                cmd.Parameters.AddWithValue("@layer", layer);
                cmd.Parameters.AddWithValue("@date", date);
                cmd.Parameters.AddWithValue("@inserts", inserts);
                cmd.Parameters.AddWithValue("@updates", updates);
                cmd.Parameters.AddWithValue("@deletes", deletes);

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        private void checkArgument(bool condition, string paramName)
        {
            if (!condition)
            {
                throw new ArgumentException(paramName);
            }
        }

        private string getInvariantSQLDate(DateTime date)
        {
            var monthString = date.Month + "";
            monthString = (monthString.Length == 2 ? "" : "0") + monthString;

            var dayString = date.Day + "";
            dayString = (dayString.Length == 2 ? "" : "0") + dayString;

            return date.Year + "" + monthString + dayString;
        }
    }
}
