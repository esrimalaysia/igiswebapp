﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGISAdmin.Models
{
    public class UserActivityByRole
    {
        public string label { get; set; }
        public int accessDays { get; set; }
        public int insertCounts { get; set; }
        public int updateCounts { get; set; }
        public int deleteCounts { get; set; }
    }

    public class UserActivityByLayer
    {
        public string label { get; set; }
        public int insertCounts { get; set; }
        public int updateCounts { get; set; }
        public int deleteCounts { get; set; }
    }

    public class ActivityBreakdown
    {
        public string label { get; set; }
        public int insertCounts { get; set; }
        public int updateCounts { get; set; }
        public int deleteCounts { get; set; }
    }
}

