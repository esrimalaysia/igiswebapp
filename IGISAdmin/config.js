﻿define(
    {
        "arcgisSvrUrl": "https://igis.iwk.com.my/geoserver",
        "editExpireMinutes": 120,
        "privilegeMap": [{
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS",
            "rolename": "HQ_Edit",
            "webmap": "IGIS",
            "webmapRef": "58aa47578435429a8a378fd10bcd8806"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS",
            "rolename": "HQ_View",
            "webmap": "IGIS_View",
            "webmapRef": "d86e577cc23d455c99f57c52039c4324"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_PenangIsland",
            "rolename": "UO_Penang_View",
            "webmap": "IGIS_PenangIsland_View",
            "webmapRef": "76b862976aee4d51915f04b4cbd5732f"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_PenangIsland",
            "rolename": "UO_Penang_Edit",
            "webmap": "IGIS_PenangIsland_Edit",
            "webmapRef": "9c8d8ad3e0aa413bb43d1dfff987e5c7"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Perai",
            "rolename": "UO_Perai_View",
            "webmap": "IGIS_Perai_View",
            "webmapRef": "f0ea43d78ef142eba19427c71b4a8a62"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Perai",
            "rolename": "UO_Perai_Edit",
            "webmap": "IGIS_Perai_Edit",
            "webmapRef": "e790818724aa41f0bb2febbf5c8e75ad"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Langkawi",
            "rolename": "UO_Langkawi_View",
            "webmap": "IGIS_Langkawi_View",
            "webmapRef": "097a22db8db04fae8ca2f6d26bbdde1e"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Langkawi",
            "rolename": "UO_Langkawi_Edit",
            "webmap": "IGIS_Langkawi_Edit",
            "webmapRef": "3652191aa4a643ac9ce70fa68224d805"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_AlorSetar",
            "rolename": "UO_AlorSetar_View",
            "webmap": "IGIS_AlorSetar_View",
            "webmapRef": "e86e5c1de91a46be88814d0aec4b8fbe"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_AlorSetar",
            "rolename": "UO_AlorSetar_Edit",
            "webmap": "IGIS_AlorSetar_Edit",
            "webmapRef": "2d2a99addb1b4db6817f9d885855e5d8"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_SgPetani",
            "rolename": "UO_SgPetani_View",
            "webmap": "IGIS_SgPetani_View",
            "webmapRef": "6812736a830d4ce8a5f5514008f3f0d4"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_SgPetani",
            "rolename": "UO_SgPetani_Edit",
            "webmap": "IGIS_SgPetani_Edit",
            "webmapRef": "cb29651b3d0f4c69a22dc24db611e981"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Ipoh",
            "rolename": "UO_Ipoh_View",
            "webmap": "IGIS_Ipoh_View",
            "webmapRef": "33681461ea7e45f19510b5d81c3e6c6b"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Ipoh",
            "rolename": "UO_Ipoh_Edit",
            "webmap": "IGIS_Ipoh_Edit",
            "webmapRef": "0cf63dd9a3ab445fbc98004cbf0ffc50"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Taiping",
            "rolename": "UO_Taiping_View",
            "webmap": "IGIS_Taiping_View",
            "webmapRef": "736c49ef83de4f508c7013f9096685fe"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Taiping",
            "rolename": "UO_Taiping_Edit",
            "webmap": "IGIS_Taiping_Edit",
            "webmapRef": "d267edc8c89242afa7bb019641784564"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Manjung",
            "rolename": "UO_Manjung_View",
            "webmap": "IGIS_Manjung_View",
            "webmapRef": "21986f25eb7b41a6addba531dc4e1d5b"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Manjung",
            "rolename": "UO_Manjung_Edit",
            "webmap": "IGIS_Manjung_Edit",
            "webmapRef": "16d5ebe4f38f4554b503747ae57c4a35"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Pahang",
            "rolename": "UO_Pahang_View",
            "webmap": "IGIS_Pahang_View",
            "webmapRef": "d2825fd7fcf64da1993a16be0e025eff"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Pahang",
            "rolename": "UO_Pahang_Edit",
            "webmap": "IGIS_Pahang_Edit",
            "webmapRef": "1ce5e6966f2d4235b4f8cb83378374b2"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Terengganu",
            "rolename": "UO_Terengganu_View",
            "webmap": "IGIS_Terengganu_View",
            "webmapRef": "b0dcbcd270de4d108cb588fd340d651f"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Terengganu",
            "rolename": "UO_Terengganu_Edit",
            "webmap": "IGIS_Terengganu_Edit",
            "webmapRef": "c6402a7f1e524abca5441341d4943cfc"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_KualaLumpur",
            "rolename": "UO_KualaLumpur_View",
            "webmap": "IGIS_KualaLumpur_View",
            "webmapRef": "223749502a274e7fb6e5552f97cf3551"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_KualaLumpur",
            "rolename": "UO_KualaLumpur_Edit",
            "webmap": "IGIS_KualaLumpur_Edit",
            "webmapRef": "2c52423e7b7142f6939d3ff5454c8bcb"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Gombak",
            "rolename": "UO_Gombak_View",
            "webmap": "IGIS_Gombak_View",
            "webmapRef": "77a1448e182b4b11bbe1eb9aac9eea87"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Gombak",
            "rolename": "UO_Gombak_Edit",
            "webmap": "IGIS_Gombak_Edit",
            "webmapRef": "e635afbc1f7a4b4ea01a52c314447fe7"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_SubangJaya",
            "rolename": "UO_SubangJaya_View",
            "webmap": "IGIS_SubangJaya_View",
            "webmapRef": "61a12f9ed41d488e9f18a68e3b3b00c9"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_SubangJaya",
            "rolename": "UO_SubangJaya_Edit",
            "webmap": "IGIS_SubangJaya_Edit",
            "webmapRef": "ad7327b0707746e3b7a2446c87d4245e"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_ShahAlam",
            "rolename": "UO_ShahAlam_View",
            "webmap": "IGIS_ShahAlam_View",
            "webmapRef": "868a626b598348b1a0881021cf398043"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_ShahAlam",
            "rolename": "UO_ShahAlam_Edit",
            "webmap": "IGIS_ShahAlam_Edit",
            "webmapRef": "b2c882b49792483e9aa8c2c95d31e7e3"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Klang",
            "rolename": "UO_Klang_View",
            "webmap": "IGIS_Klang_View",
            "webmapRef": "9c170619b7f34f799494297e3e0d5b29"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Klang",
            "rolename": "UO_Klang_Edit",
            "webmap": "IGIS_Klang_Edit",
            "webmapRef": "c6ed1ab190e64e2687c37f206cd4e508"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Labuan",
            "rolename": "UO_Labuan_View",
            "webmap": "IGIS_Labuan_View",
            "webmapRef": "716bff36173e44b39d1e40b4a4e9f2f6"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Labuan",
            "rolename": "UO_Labuan_Edit",
            "webmap": "IGIS_Labuan_Edit",
            "webmapRef": "aa772990af64495daf05a5d61c173eef"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Langat",
            "rolename": "UO_Langat_View",
            "webmap": "IGIS_Langat_View",
            "webmapRef": "7384d08d6183405998655a11792979f9"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Langat",
            "rolename": "UO_Langat_Edit",
            "webmap": "IGIS_Langat_Edit",
            "webmapRef": "8f0ea932880b4f06be439713c87f58d3"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Melaka",
            "rolename": "UO_Melaka_View",
            "webmap": "IGIS_Melaka_View",
            "webmapRef": "6310501f29c646218613d11e717c3a71"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Melaka",
            "rolename": "UO_Melaka_Edit",
            "webmap": "IGIS_Melaka_Edit",
            "webmapRef": "a919ad63c3b9478098a4870a0898068e"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_N9",
            "rolename": "UO_N9_View",
            "webmap": "IGIS_N9_View",
            "webmapRef": "c31dc490f57844cb8a1f791722dbf272"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_N9",
            "rolename": "UO_N9_Edit",
            "webmap": "IGIS_N9_Edit",
            "webmapRef": "99792bcdb85348a5b42183b29035c971"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Skudai",
            "rolename": "UO_Skudai_View",
            "webmap": "IGIS_Skudai_View",
            "webmapRef": "973c6f0f27394bb685c1b77ccfaf0885"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Skudai",
            "rolename": "UO_Skudai_Edit",
            "webmap": "IGIS_Skudai_Edit",
            "webmapRef": "6c56bbe749d2433d8b4c3ae38dc47370"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Kluang",
            "rolename": "UO_Kluang_View",
            "webmap": "IGIS_Kluang_View",
            "webmapRef": "8115404d93a246109d8dd0211257a7bc"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Kluang",
            "rolename": "UO_Kluang_Edit",
            "webmap": "IGIS_Kluang_Edit",
            "webmapRef": "d39ee6967131465f860450c572ae453f"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_FedTerr",
            "rolename": "CA_FedTerr_View",
            "webmap": "IGIS_FedTerr_View",
            "webmapRef": "78a9d75835744df884dcd5fade2fabd0"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_FedTerr",
            "rolename": "CA_FedTerr_Edit",
            "webmap": "IGIS_FedTerr_Edit",
            "webmapRef": "5c604e8d4c6e48a49e0b5d0807ebe424"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_KedahPerlis",
            "rolename": "CA_KedahPerlis_View",
            "webmap": "IGIS_KedahPerlis_View",
            "webmapRef": "8789e2d60dfe486d9435c8c0cdc40c3d"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_KedahPerlis",
            "rolename": "CA_KedahPerlis_Edit",
            "webmap": "IGIS_KedahPerlis_Edit",
            "webmapRef": "1bf87e73a5dd451db68088df5657479e"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_PenangState",
            "rolename": "CA_Penang_View",
            "webmap": "IGIS_PenangCA_View",
            "webmapRef": "ec4bd74556c2431493c3f4b736b629f4"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_PenangState",
            "rolename": "CA_Penang_Edit",
            "webmap": "IGIS_PenangCA_Edit",
            "webmapRef": "5abed113cf4c4ec4a78f1749dd923538"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Perak",
            "rolename": "CA_Perak_View",
            "webmap": "IGIS_PerakCA_View",
            "webmapRef": "42d856ef71894cee9d0b540b2f62aa85"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Perak",
            "rolename": "CA_Perak_Edit",
            "webmap": "IGIS_PerakCA_Edit",
            "webmapRef": "2cc1560231404b788f666470a9ea4ad1"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_N9",
            "rolename": "CA_N9_View",
            "webmap": "IGIS_N9CA_View",
            "webmapRef": "a205c383298343e3ba1a446909312d81"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_N9",
            "rolename": "CA_N9_Edit",
            "webmap": "IGIS_N9CA_Edit",
            "webmapRef": "04f8506bdde04875b781776d0b54435b"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Melaka",
            "rolename": "CA_Melaka_View",
            "webmap": "IGIS_MelakaCA_View",
            "webmapRef": "336badef37b24eacbaed24668feabb77"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Melaka",
            "rolename": "CA_Melaka_Edit",
            "webmap": "IGIS_MelakaCA_Edit",
            "webmapRef": "a9597da963244098b9a4fa7894650eb8"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Johor",
            "rolename": "CA_Johor_View",
            "webmap": "IGIS_Johor_View",
            "webmapRef": "0dabc01a69c549e1ba6e9741105b004a"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Johor",
            "rolename": "CA_Johor_Edit",
            "webmap": "IGIS_Johor_Edit",
            "webmapRef": "06ee6634040f4ec5b76303a081b0357f"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Pahang",
            "rolename": "CA_Pahang_View",
            "webmap": "IGIS_PahangCA_View",
            "webmapRef": "914b0ca8b46a4077be2ea0448ac63fc6"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Pahang",
            "rolename": "CA_Pahang_Edit",
            "webmap": "IGIS_PahangCA_Edit",
            "webmapRef": "6d47a6356ab3479cbb3b739ec496de75"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Terengganu",
            "rolename": "CA_Terengganu_View",
            "webmap": "IGIS_TerengganuCA_View",
            "webmapRef": "949ed1c4d51d4d3e94506c722b083460"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Terengganu",
            "rolename": "CA_Terengganu_Edit",
            "webmap": "IGIS_TerengganuCA_Edit",
            "webmapRef": "4bb8b1cde3c84457ba07a2d97554ddb5"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Selangor",
            "rolename": "CA_Selangor_View",
            "webmap": "IGIS_Selangor_View",
            "webmapRef": "1ef9eead2c5842bb86688c76d548f50a"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "New Development"
            }],
            "region": "IGIS_Selangor",
            "rolename": "CA_Selangor_Edit",
            "webmap": "IGIS_Selangor_Edit",
            "webmapRef": "6f548854dbca4e518ab7751edfe8ffe7"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Northern",
            "rolename": "PU_Northern_View",
            "webmap": "IGIS_Northern_View",
            "webmapRef": "e69b5c817efb45509c13f5724abfb433"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Northern",
            "rolename": "PU_Northern_Edit",
            "webmap": "IGIS_Northern_Edit",
            "webmapRef": "d3bd27a6cebc4a2fb1a15e60b6163c28"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Southern",
            "rolename": "PU_Southern_View",
            "webmap": "IGIS_Southern_View",
            "webmapRef": "9a56ae13b2e642a58ba3750afe508852"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Southern",
            "rolename": "PU_Southern_Edit",
            "webmap": "IGIS_Southern_Edit",
            "webmapRef": "b7322d2d0b9541b48b1f54ed9592a251"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Eastern",
            "rolename": "PU_Eastern_View",
            "webmap": "IGIS_Eastern_View",
            "webmapRef": "7fc430fa47934dbbb4ae132b761173d6"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Eastern",
            "rolename": "PU_Eastern_Edit",
            "webmap": "IGIS_Eastern_Edit",
            "webmapRef": "289ab4130819491e94d3d37c05ca9053"
        },
        {
            "layers": [{
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": false,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": false,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Central",
            "rolename": "PU_Central_View",
            "webmap": "IGIS_Central_View",
            "webmapRef": "548274898f784528ac2e56aaec60863d"
        },
        {
            "layers": [{
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Point)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Operation (Line)"
            },
            {
                "canEdit": true,
                "canView": true,
                "layer": "Risk Sewerline"
            },
            {
                "canEdit": true,
                "canView": false,
                "layer": "New Development"
            }],
            "region": "IGIS_Central",
            "rolename": "PU_Central_Edit",
            "webmap": "IGIS_Central_Edit",
            "webmapRef": "54af1396f1b748ba968e76544a2cf9ab"
        }],
        "regionGisLayer": "https://igis.iwk.com.my/geoserver/rest/services/Production/IGIS_Base/MapServer/18",
        "regionMap": [{
            "region": "IGIS"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP PULAU PINANG"]
            }],
            "region": "IGIS_PenangIsland"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP SEBERANG PERAI"]
            }],
            "region": "IGIS_Perai"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP LANGKAWI"]
            }],
            "region": "IGIS_Langkawi"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MB KOTA SETAR",
                    "MD KUBANG PASU",
                    "MD PADANG TERAP",
                    "MD PENDANG",
                    "MD YAN",
                    "MP KANGAR"]
            }],
            "region": "IGIS_AlorSetar"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP SUNGAI PETANI",
                    "MD KULIM",
                    "MD SIK",
                    "MD BALING",
                    "MD BANDAR BAHARU"]
            }],
            "region": "IGIS_SgPetani"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MB IPOH"]
            }],
            "region": "IGIS_Ipoh"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP TAIPING",
                    "MD PENGKALAN HULU",
                    "MD GRIK",
                    "MD SELAMA",
                    "MD LENGGONG",
                    "MP KUALA KANGSAR",
                    "MD KERIAN"]
            }],
            "region": "IGIS_Taiping"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MD PERAK TENGAH",
                    "MD BATU GAJAH",
                    "MD KAMPAR",
                    "MP MANJUNG",
                    "MP TELUK INTAN",
                    "MD TAPAH",
                    "MD TANJUNG MALIM"]
            }],
            "region": "IGIS_Manjung"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP KUANTAN",
                    "MD CAMERON HIGHLANDS",
                    "MD LIPIS",
                    "MD JERANTUT",
                    "MD RAUB",
                    "MP TEMERLOH",
                    "MP BENTONG",
                    "MD PEKAN",
                    "MD ROMPIN",
                    "MD BERA"]
            }],
            "region": "IGIS_Pahang"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP KUALA TERENGGANU",
                    "MD BESUT",
                    "MD SETIU",
                    "MD MARANG",
                    "MD HULU TERENGGANU",
                    "MD DUNGUN",
                    "MP KEMAMAN"]
            }],
            "region": "IGIS_Terengganu"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP AMPANG JAYA",
                    "MP SELAYANG",
                    "MD SABAK BERNAM",
                    "MD HULU SELANGOR",
                    "MP SUBANG JAYA",
                    "MD KUALA LANGAT",
                    "MB SHAH ALAM",
                    "MB PETALING JAYA",
                    "MP KLANG",
                    "MD KUALA SELANGOR",
                    "MP KAJANG",
                    "MP SEPANG"]
            }],
            "region": "IGIS_Selangor"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["DEWAN BANDARAYA KUALA LUMPUR",
                    "PERBADANAN PUTRAJAYA"]
            }],
            "region": "IGIS_KualaLumpur"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP AMPANG JAYA",
                    "MP SELAYANG",
                    "MD SABAK BERNAM",
                    "MD HULU SELANGOR"]
            }],
            "region": "IGIS_Gombak"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP SUBANG JAYA",
                    "MD KUALA LANGAT"]
            }],
            "region": "IGIS_SubangJaya"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MB SHAH ALAM",
                    "MB PETALING JAYA"]
            }],
            "region": "IGIS_ShahAlam"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP KLANG",
                    "MD KUALA SELANGOR"]
            }],
            "region": "IGIS_Klang"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP KAJANG",
                    "MP SEPANG"]
            }],
            "region": "IGIS_Langat"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["PERBADANAN LABUAN"]
            }],
            "region": "IGIS_Labuan"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MB MELAKA BERSEJARAH",
                    "MD JASIN",
                    "MP ALOR GAJAH",
                    "MP HANG TUAH JAYA"]
            }],
            "region": "IGIS_Melaka"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP SEREMBAN",
                    "MD JELEBU",
                    "MD JEMPOL",
                    "MD KUALA PILAH",
                    "MD REMBAU",
                    "MP NILAI",
                    "MP PORT DICKSON",
                    "MD TAMPIN"]
            }],
            "region": "IGIS_N9"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MD PONTIAN",
                    "MP KULAI",
                    "MP JOHOR BAHRU TENGAH",
                    "MD KOTA TINGGI",
                    "MD MERSING",
                    "PBT PENGERANG"]
            }],
            "region": "IGIS_Skudai"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MD SEGAMAT",
                    "MD LABIS",
                    "MD TANGKAK",
                    "MP MUAR",
                    "MP KLUANG",
                    "MD SIMPANG RENGGAM",
                    "MD YONG PENG",
                    "MP BATU PAHAT"]
            }],
            "region": "IGIS_Kluang"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["DEWAN BANDARAYA KUALA LUMPUR",
                    "PERBADANAN PUTRAJAYA",
                    "PERBADANAN LABUAN"]
            }],
            "region": "IGIS_FedTerr"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP LANGKAWI",
                    "MB KOTA SETAR",
                    "MD KUBANG PASU",
                    "MD PADANG TERAP",
                    "MD PENDANG",
                    "MD YAN",
                    "MP KANGAR",
                    "MP SUNGAI PETANI",
                    "MD KULIM",
                    "MD SIK",
                    "MD BALING",
                    "MD BANDAR BAHARU"]
            }],
            "region": "IGIS_KedahPerlis"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP PULAU PINANG",
                    "MP SEBERANG PERAI"]
            }],
            "region": "IGIS_PenangState"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MB IPOH",
                    "MP TAIPING",
                    "MD PENGKALAN HULU",
                    "MD GRIK",
                    "MD SELAMA",
                    "MD LENGGONG",
                    "MP KUALA KANGSAR",
                    "MD KERIAN",
                    "MD PERAK TENGAH",
                    "MD BATU GAJAH",
                    "MD KAMPAR",
                    "MP MANJUNG",
                    "MP TELUK INTAN",
                    "MD TAPAH",
                    "MD TANJUNG MALIM"]
            }],
            "region": "IGIS_Perak"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MD PONTIAN",
                    "MP KULAI",
                    "MP JOHOR BAHRU TENGAH",
                    "MD KOTA TINGGI",
                    "MD MERSING",
                    "PBT PENGERANG",
                    "MD SEGAMAT",
                    "MD LABIS",
                    "MD TANGKAK",
                    "MP MUAR",
                    "MP KLUANG",
                    "MD SIMPANG RENGGAM",
                    "MD YONG PENG",
                    "MP BATU PAHAT"]
            }],
            "region": "IGIS_Johor"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP LANGKAWI",
                    "MB KOTA SETAR",
                    "MD KUBANG PASU",
                    "MD PADANG TERAP",
                    "MD PENDANG",
                    "MD YAN",
                    "MP KANGAR",
                    "MP SUNGAI PETANI",
                    "MD KULIM",
                    "MD SIK",
                    "MD BALING",
                    "MD BANDAR BAHARU",
                    "MP PULAU PINANG",
                    "MP SEBERANG PERAI",
                    "MB IPOH",
                    "MP TAIPING",
                    "MD PENGKALAN HULU",
                    "MD GRIK",
                    "MD SELAMA",
                    "MD LENGGONG",
                    "MP KUALA KANGSAR",
                    "MD KERIAN",
                    "MD PERAK TENGAH",
                    "MD BATU GAJAH",
                    "MD KAMPAR",
                    "MP MANJUNG",
                    "MP TELUK INTAN",
                    "MD TAPAH",
                    "MD TANJUNG MALIM"]
            }],
            "region": "IGIS_Northern"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MD PONTIAN",
                    "MP KULAI",
                    "MP JOHOR BAHRU TENGAH",
                    "MD KOTA TINGGI",
                    "MD MERSING",
                    "PBT PENGERANG",
                    "MD SEGAMAT",
                    "MD LABIS",
                    "MD TANGKAK",
                    "MP MUAR",
                    "MP KLUANG",
                    "MD SIMPANG RENGGAM",
                    "MD YONG PENG",
                    "MP BATU PAHAT",
                    "MP SEREMBAN",
                    "MD JELEBU",
                    "MD JEMPOL",
                    "MD KUALA PILAH",
                    "MD REMBAU",
                    "MP NILAI",
                    "MP PORT DICKSON",
                    "MD TAMPIN",
                    "MB MELAKA BERSEJARAH",
                    "MD JASIN",
                    "MP ALOR GAJAH",
                    "MP HANG TUAH JAYA"]
            }],
            "region": "IGIS_Southern"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["MP KUANTAN",
                    "MD CAMERON HIGHLANDS",
                    "MD LIPIS",
                    "MD JERANTUT",
                    "MD RAUB",
                    "MP TEMERLOH",
                    "MP BENTONG",
                    "MD PEKAN",
                    "MD ROMPIN",
                    "MD BERA",
                    "MP KUALA TERENGGANU",
                    "MD BESUT",
                    "MD SETIU",
                    "MD MARANG",
                    "MD HULU TERENGGANU",
                    "MD DUNGUN",
                    "MP KEMAMAN"]
            }],
            "region": "IGIS_Eastern"
        },
        {
            "filter": [{
                "field": "LA_NAME",
                "values": ["DEWAN BANDARAYA KUALA LUMPUR",
                    "PERBADANAN PUTRAJAYA",
                    "PERBADANAN LABUAN",
                    "MP AMPANG JAYA",
                    "MP SELAYANG",
                    "MD SABAK BERNAM",
                    "MD HULU SELANGOR",
                    "MP SUBANG JAYA",
                    "MD KUALA LANGAT",
                    "MB SHAH ALAM",
                    "MB PETALING JAYA",
                    "MP KLANG",
                    "MD KUALA SELANGOR",
                    "MP KAJANG",
                    "MP SEPANG"]
            }],
            "region": "IGIS_Central"
        }]
    }
);