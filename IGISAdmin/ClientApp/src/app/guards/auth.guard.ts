import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JwtHelper } from "angular2-jwt";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let currentUser = JSON.parse(localStorage.getItem('currentIgisAdminUser'));
    //console.log(currentUser);
    if (currentUser && currentUser.token) {
      const helper = new JwtHelper();
      const decodedToken = helper.decodeToken(currentUser.token);
      //console.log(decodedToken);
      // if stored token expiry is before date.now (expressed in milliseconds vis-a-vis seconds in token expiry) , proceed to login
      if (decodedToken.exp < Date.now()/1000 ) { 
        console.log("Token expired, proceed to login ..");
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
      }
      return true;
    }
    else {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }
}
