import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'expand-collapse-button',
  templateUrl: './expandcollapsebutton.component.html'
})
export class ExpandCollapseButtonComponent {
  @Input('vertical') public vertical: boolean = true;
  @Input('isExpanded') public _isExpanded: boolean = false;
  @Input('isLoading') public _isLoading: boolean = false;
  @Output('onChange') public _onChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  public constructor() {
  }

  public onToggle(): void {
    if (!this._isLoading) {
      this._onChange.emit(!this._isExpanded);
    }
  }
}
