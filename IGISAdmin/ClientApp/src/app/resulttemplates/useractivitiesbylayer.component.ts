import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { UserActivityReportsService } from '../services/useractivityreports.service';
import { Moment } from 'moment';

@Component({
  selector: 'user-activities-by-layer',
  templateUrl: './useractivitiesbylayer.component.html',
  styleUrls: ['./useractivitiesbylayer.component.css']
})
export class UserActivitiesByLayerComponent {
  @Input('data') public activitiesByLayer: ActivityByLayerUI[] | undefined = undefined;
  @Input() protected fromDate: Moment | undefined = undefined;
  @Input() protected toDate: Moment | undefined = undefined;
  @Input() protected isSingleRole: boolean = false;

  public constructor(private _userActivityReportsService: UserActivityReportsService) {
    console.log('User Activity by layer component');
  }

  public onExpandLayer(activities: ActivityByLayerUI, isExpanded: boolean): void {
    if (activities.isExpanded) {
      activities.isExpanded = false;
    } else {
      activities.isLoading = true;
      this._userActivityReportsService
        .queryRoleBreakdownByLayer(this.fromDate, this.toDate, activities.label)
        .then((changes: LabelChanges[]) => {
          changes.forEach((value) => value.isExpanded = true);
          activities.roleChanges = changes;
          activities.isExpanded = true;
          activities.isLoading = false;
        });
    }
  }
}
