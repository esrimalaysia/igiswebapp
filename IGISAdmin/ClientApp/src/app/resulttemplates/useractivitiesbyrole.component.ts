import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { UserActivityReportsService } from '../services/useractivityreports.service';
import { Attribute } from '@angular/compiler';
import { Moment } from 'moment';

@Component({
  selector: 'user-activities-by-role',
  templateUrl: './useractivitiesbyrole.component.html',
  styleUrls: ['./useractivitiesbyrole.component.css']
})
export class UserActivitiesByRoleComponent {

  @Input('data') public activitiesByRole: ActivityByRoleUI[] | undefined = undefined;
  @Input() protected fromDate: Moment | undefined = undefined;
  @Input() protected toDate: Moment | undefined = undefined;
  @Input() public isSingleLayer: boolean = false;
  @Input() protected isSingleRole: boolean = false;
  private _showChangesColumn = false;

  public constructor(private _userActivityReportsService: UserActivityReportsService) {
  }

  public onExpandRole(activities: ActivityByRoleUI, isExpanded: boolean): void {
    if (activities.isExpanded) {
      activities.isExpanded = false;
    } else {
      activities.isLoading = true;

      if (this.isSingleRole) {
        this._userActivityReportsService
          .queryLayerBreakdownByUser(this.fromDate!, this.toDate!, activities.label)
          .then((changes: LabelChanges[]) => {
            activities.layerChanges = changes;
            activities.isExpanded = true;
            activities.isLoading = false;
          });
      }
      else {
        this._userActivityReportsService
          .queryLayerBreakdownByRole(this.fromDate!, this.toDate!, activities.label)
          .then((changes: LabelChanges[]) => {
            activities.layerChanges = changes;
            activities.isExpanded = true;
            activities.isLoading = false;
          });
      }
    }
  }

  public onExpandSingleLayerRoleChanges(): void {
    this._showChangesColumn = !this._showChangesColumn;
  }

  public onExpandLayer(layerChanges: LayerChangesUI): void {
    layerChanges.isExpanded = !layerChanges.isExpanded;
    console.log(layerChanges);
  }
}
