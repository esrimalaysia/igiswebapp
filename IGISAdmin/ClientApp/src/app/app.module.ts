import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatDatepickerModule, MatButtonModule, MatListModule, MatDividerModule, MatSelectModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ExpandCollapseButtonComponent } from './expandcollapsebutton/expandcollapsebutton.component';
import { UserActivityReportsComponent } from './useractivityreports/useractivityreports.component';
import { UserActivitiesByLayerComponent } from './resulttemplates/useractivitiesbylayer.component';
import { UserActivitiesByRoleComponent } from './resulttemplates/useractivitiesbyrole.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ExpandCollapseButtonComponent,
    UserActivityReportsComponent,
    UserActivitiesByLayerComponent,
    UserActivitiesByRoleComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,

    // Material
    NoopAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatSelectModule,

    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'useractivityreports', component: UserActivityReportsComponent, canActivate: [AuthGuard] }
    ])
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
