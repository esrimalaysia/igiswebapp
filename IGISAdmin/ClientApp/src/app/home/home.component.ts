import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AuthenticationService]
})
export class HomeComponent {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  onLogoutClick() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
  

}
