import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { getQueryValue } from "@angular/core/src/view/query";
import { Moment } from "moment";

@Injectable()
export class UserActivityReportsService {
  private static readonly URL_BASE: string = 'api/useractivity';
  private static readonly URL_GET_ALL_ROLES: string = UserActivityReportsService.URL_BASE + '/roles';
  private static readonly URL_GET_ALL_LAYERS: string = UserActivityReportsService.URL_BASE + '/layers';
  private static readonly URL_GET_ALL_USERS: string = UserActivityReportsService.URL_BASE + '/users';
  private static readonly URL_GET_BREAKDOWN_BY_ROLE = (role: string): string => `${UserActivityReportsService.URL_GET_ALL_ROLES}/${role}/layers`;
  private static readonly URL_GET_BREAKDOWN_BY_LAYER = (layer: string): string => `${UserActivityReportsService.URL_GET_ALL_LAYERS}/${layer}/roles`;
  private static readonly URL_GET_BREAKDOWN_BY_USER = (user: string): string => `${UserActivityReportsService.URL_GET_ALL_USERS}/${user}/layers`;
  private static readonly DATE_FORMAT = 'YYYY-MM-DD';

  public constructor(private http: HttpClient) {
  }

  public queryAllRole(): Promise<QuerySelectionUI[]> {
    return new Promise((fulfill) => {
      this.http.get(UserActivityReportsService.URL_GET_ALL_ROLES).subscribe((values) => {
        let result: QuerySelectionUI[] = [];
        if (!!values && Array.isArray(values)) {
          result.push({ label: 'All Roles', value: '' });
          for (let value of values) {
            result.push({
              label: value.label,
              value: value.label,
            });
          }
        }

        fulfill(result);
      });
    });
  }

  public getLayerDefaultSelection(): QuerySelectionUI[] {
    return [{ label: 'All Layers', value: '' }];
  }

  public queryAllLayer(): Promise<QuerySelectionUI[]> {
    return new Promise((fulfill) => {
      this.http.get(UserActivityReportsService.URL_GET_ALL_LAYERS).subscribe((values) => {
        let result: QuerySelectionUI[] = this.getLayerDefaultSelection();
        if (!!values && Array.isArray(values)) {
          for (let value of values) {
            result.push({
              label: value.label,
              value: value.label,
            });
          }
        }

        fulfill(result);
      });
    });
  }

  public queryAllQueryType(): Promise<QuerySelectionUI[]> {
    return new Promise((fulfill) => {
      fulfill([
        { label: 'Role/User', value: 'role' },
        { label: 'Layer', value: 'layer' },
      ]);
    });
  }

  private getQueryString(arr: string[][]): string {
    let qString = '?';
    for (let item of arr) {
      qString += item[0] + '=' + item[1] + '&';
    }

    return qString.substr(0, qString.length - 1);
  }

  public queryUserActivitiesByRole(startDate: Moment, endDate: Moment, role: string, layer: string): Promise<ActivityByRole[]> {
    return new Promise((fulfill) => {
      let url = UserActivityReportsService.URL_BASE + this.getQueryString([
        ['start', startDate.format(UserActivityReportsService.DATE_FORMAT)],
        ['end', endDate.format(UserActivityReportsService.DATE_FORMAT)],
        ['role', !!role ? role : ''],
        ['layer', !!layer ? layer : ''],
        ['groupby', 'role']
      ]);

      this.http.get(url).subscribe((values) => {
        let result: ActivityByRole[] = [];
        if (!!values && Array.isArray(values)) {
          for (let value of values) {
            result.push({
              label: value.label,
              accessDays: value.accessDays,
              changes: {
                insertCounts: value.insertCounts,
                updateCounts: value.updateCounts,
                deleteCounts: value.deleteCounts
              }
            });
          }
        }

        fulfill(result);
      });
    });
  }

  public queryUserActivitiesByLayer(startDate: Moment, endDate: Moment, role: string, layer: string): Promise<ActivityByLayer[]> {
    return new Promise((fulfill) => {
      let url = UserActivityReportsService.URL_BASE + this.getQueryString([
        ['start', startDate.format(UserActivityReportsService.DATE_FORMAT)],
        ['end', endDate.format(UserActivityReportsService.DATE_FORMAT)],
        ['role', !!role ? role : ''],
        ['layer', !!layer ? layer : ''],
        ['groupby', 'layer']
      ]);

      this.http.get(url).subscribe((values) => {
        let result: ActivityByLayer[] = [];
        if (!!values && Array.isArray(values)) {
          for (let value of values) {
            result.push({
              label: value.label,
              insertCounts: value.insertCounts,
              updateCounts: value.updateCounts,
              deleteCounts: value.deleteCounts,
            });
          }
        }
        console.log(result);
        fulfill(result);
      });
    });
  }

  public queryLayerBreakdownByRole(startDate: Moment, endDate: Moment, role: string): Promise<LabelChanges[]> {
    return new Promise((fulfill) => {
      let url = UserActivityReportsService.URL_GET_BREAKDOWN_BY_ROLE(role) + this.getQueryString([
        ['start', startDate.format(UserActivityReportsService.DATE_FORMAT)],
        ['end', endDate.format(UserActivityReportsService.DATE_FORMAT)]]
      );

      this.http.get(url).subscribe((values) => {
        let result: LabelChanges[] = [];
        if (!!values && Array.isArray(values)) {
          for (let value of values) {
            result.push({
              label: value.label,
              changes: {
                insertCounts: value.insertCounts,
                updateCounts: value.updateCounts,
                deleteCounts: value.deleteCounts
              },
            });
          }
        }

        fulfill(result);
      });
    });
  }

  public queryRoleBreakdownByLayer(startDate: Moment, endDate: Moment, layer: string): Promise<LabelChanges[]> {
    return new Promise((fulfill) => {
      let url = UserActivityReportsService.URL_GET_BREAKDOWN_BY_LAYER(layer) + this.getQueryString([
        ['start', startDate.toISOString().substr(0, 10)],
        ['end', endDate.toISOString().substr(0, 10)]]
      );

      this.http.get(url).subscribe((values) => {
        let result: LabelChanges[] = [];
        if (!!values && Array.isArray(values)) {
          for (let value of values) {
            result.push({
              label: value.label,
              changes: {
                insertCounts: value.insertCounts,
                updateCounts: value.updateCounts,
                deleteCounts: value.deleteCounts
              },
            });
          }
        }

        fulfill(result);
      });
    });
  }

  public queryLayerBreakdownByUser(startDate: Moment, endDate: Moment, user: string): Promise<LabelChanges[]> {
    return new Promise((fulfill) => {
      let url = UserActivityReportsService.URL_GET_BREAKDOWN_BY_USER(user) + this.getQueryString([
        ['start', startDate.toISOString().substr(0, 10)],
        ['end', endDate.toISOString().substr(0, 10)]]
      );

      this.http.get(url).subscribe((values) => {
        let result: LabelChanges[] = [];
        if (!!values && Array.isArray(values)) {
          for (let value of values) {
            result.push({
              label: value.label,
              changes: {
                insertCounts: value.insertCounts,
                updateCounts: value.updateCounts,
                deleteCounts: value.deleteCounts
              },
            });
          }
        }

        fulfill(result);
      });
    });
  }



}
