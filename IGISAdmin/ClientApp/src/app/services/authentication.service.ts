import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { catchError, retry, tap } from 'rxjs/operators';

import { map } from 'rxjs/operator/map';

@Injectable()
export class AuthenticationService {
  private readonly AUTH_ROUTE: string = 'api/account/gettoken';

  constructor(private http: HttpClient) { }

  public login(username: string, password: string): Observable<LoginResult>{

    return this.http.post<LoginResult>(this.AUTH_ROUTE, { username: username, password: password })
      .pipe(
        tap(data => {
        if (data && data.success && data.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentIgisAdminUser', JSON.stringify(data));
          }
        })
      );
  }

  public logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentIgisAdminUser');
  }
}
