import { Component } from '@angular/core';
import { UserActivityReportsService } from '../services/useractivityreports.service';
import * as moment from 'moment'
import { Moment } from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'useractivityreports',
  templateUrl: './useractivityreports.component.html',
  styleUrls: ['./useractivityreports.component.css'],
  providers: [UserActivityReportsService]
})
export class UserActivityReportsComponent {
  public _queryFromDate: Moment;
  public _queryToDate: Moment;
  public _querySummary = '';

  public _selectedRole: QuerySelectionUI | undefined = undefined;
  public _selectedLayer: QuerySelectionUI | undefined = undefined;
  public _selectedQueryType: QuerySelectionUI | undefined = undefined;

  public _roleSelection: QuerySelectionUI[] | undefined = undefined;
  public _layerSelection: QuerySelectionUI[] | undefined = undefined;
  public _oriLayerSelection: QuerySelectionUI[] | undefined = undefined;
  public _displayBySelection: QuerySelectionUI[] | undefined = undefined;

  public _resultFromDate: Moment | undefined = undefined;
  public _resultToDate: Moment | undefined = undefined;
  public _resultDisplayType: string = '';
  public _resultRole: QuerySelectionUI | undefined = undefined;
  public _resultLayer: QuerySelectionUI | undefined = undefined;
  public _result: ActivityByRoleUI[] | ActivityByRoleUI[] | undefined = undefined;

  public constructor(private _userActivityReportsService: UserActivityReportsService, private router: Router) {
    this._queryToDate = moment().utc();
    this._queryFromDate = moment().utc().subtract(1, 'months');

    this._userActivityReportsService.queryAllRole()
      .then((values: QuerySelectionUI[]) => {
        this._roleSelection = values;
        if (values.length > 0) {
          this._selectedRole = values[0];
        }
      });

    this._userActivityReportsService.queryAllLayer()
      .then((values: QuerySelectionUI[]) => {
        this._layerSelection = values;
        this._oriLayerSelection = values;
        if (values.length > 0) {
          this._selectedLayer = values[0];
        }
      });

    this._userActivityReportsService.queryAllQueryType()
      .then((values: QuerySelectionUI[]) => {
        this._displayBySelection = values;
        if (values.length > 0) {
          this._selectedQueryType = values[0];
        }
      });
  }

  public set displayType(displayType: QuerySelectionUI) {
    /*if (displayType.value === 'role') {
      this._layerSelection = this._oriLayerSelection;
      this._selectedLayer = this._layerSelection[0];
    } else {//display type == 'layer'
      this._layerSelection = this._userActivityReportsService.getLayerDefaultSelection();
      this._selectedLayer = this._layerSelection[0];
    }
    */
    this._selectedQueryType = displayType;
  }

  public get displayType(): QuerySelectionUI {
    return this._selectedQueryType;
  }

  public queryActivity(): void {
    let fromDate = this._queryFromDate;
    let toDate = this._queryToDate;
    let displayType = this._selectedQueryType!.value;
    let role = this._selectedRole!;
    let layer = this._selectedLayer!;

    let promise: Promise<any> = displayType === 'role'
      ? this._userActivityReportsService.queryUserActivitiesByRole(fromDate, toDate, role.value, layer.value)
      : this._userActivityReportsService.queryUserActivitiesByLayer(fromDate, toDate, role.value, layer.value);

    promise.then((value: any) => {
      this._resultFromDate = fromDate;
      this._resultToDate = toDate;
      this._resultRole = role;
      this._resultLayer = layer;
      this._resultDisplayType = displayType;
      this._result = value;
    });
  }

  onBackClick() {
    this.router.navigate(['/']);
  }
}
