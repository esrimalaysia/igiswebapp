interface Changes {
  insertCounts: number;
  updateCounts: number;
  deleteCounts: number;
}

interface LabelChanges {
  label: string;
  changes?: Changes;
  isExpanded?: boolean;
}

interface ActivityByRole {
  label: string;
  accessDays: number;
  changes?: Changes;
  layerChanges?: LabelChanges[];
}

interface ActivityByLayer extends Changes {
  label: string;
  roleChanges?: LabelChanges[];
}

interface QuerySelectionUI {
  label: string;
  value: any;
}

interface LayerChangesUI extends LabelChanges {
}

interface ActivityByRoleUI extends ActivityByRole {
  start: Date;
  end: Date;
  isExpanded: boolean;
  isLoading: boolean;
}

interface ActivityByLayerUI extends ActivityByLayer {
  start: Date;
  end: Date;
  isExpanded: boolean;
  isLoading: boolean;
}

interface LoginResult {
  success: boolean;
  errorReason: string;
  token: string;
  role: string;
}
