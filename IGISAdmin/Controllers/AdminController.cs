﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using IGISAccess.ArcgisAccess;
using IGISAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IGISAdmin.Controllers
{
    [Route("admin/[action]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IArcgisSignInManager _signInManager;
        private readonly IArcGISRoleManager _roleManager;

        public AdminController(IArcgisSignInManager signInManager, IArcGISRoleManager roleManager)
        {
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        class PrivilegeListJson
        {
            List<Privilege> privileges;
            public List<Privilege> Privileges { get => privileges; set => privileges = value; }
            public PrivilegeListJson(List<Privilege> privileges)
            {
                this.privileges = privileges;
            }
        }

        class LocalAuthoritiesJson
        {
            List<LocalAuthority> localAuthorities;
            public List<LocalAuthority> LocalAuthorityList { get => localAuthorities; set => localAuthorities = value; }
            public LocalAuthoritiesJson(List<LocalAuthority> las)
            {
                this.localAuthorities = las;
            }
        }

        [HttpGet]
        [ActionName("getprivilegelist")]
        public IActionResult GetPrivilegeList(int appid, string keyword)
        {
            if (keyword != "discovery")
            {
                return Ok();
            }
            string message;
            List<Privilege> privileges = _roleManager.GetAllPrivileges(out message, appid);
            if (!privileges.Any())
            {
                return NotFound();
            }

            return new ContentResult() { ContentType = "application/json", 
                Content= JsonSerializer.Serialize(new PrivilegeListJson(privileges))};
        }

        [HttpGet]
        [ActionName("setwebmap")]
        public IActionResult SetWebMapReference(int appid, int roleid, string webmap, string webmapref, string keyword)
        {
            if (keyword != "discovry")
            {
                return Ok();
            }
            string message;
            bool success = _roleManager.SetWebMapReference(appid, roleid, webmap, webmapref, out message);
            if (!success)
            {
                return BadRequest(message);
            }
            return Ok();
        }

        [HttpGet]
        [ActionName("getlamap")]
        public IActionResult GetLocalAuthorityMap(string keyword)
        {
            if (keyword != "lamian")
            {
                return Ok();
            }
            string message;
            List<LocalAuthority> localAuths = _roleManager.GetLocalAuthorityMap(out message);
            if (!localAuths.Any())
            {
                return NotFound();
            }

            return new ContentResult()
            {
                ContentType = "application/json",
                Content = JsonSerializer.Serialize(new LocalAuthoritiesJson(localAuths))
            };
        }
    }
}