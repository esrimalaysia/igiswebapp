﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IGISAccess;
using IGISAccess.ArcgisAccess;
using IGISAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace IGISAdmin.Controllers
{
    public class UserDto
    {
        public string username { get; set; }
        public string password { get; set; }
    }


    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IArcgisSignInManager _signInManager;
        private readonly IArcGISRoleManager _roleManager;

        public AccountController(IArcgisSignInManager signInManager, IArcGISRoleManager roleManager)
        {
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        [HttpPost("gettoken")]
        //returns jwt token
        public async Task<IActionResult> GetToken([FromBody]UserDto userDto)
        {
            ArcgisSignInResult result = await _signInManager.CheckPasswordSignInAsync(userDto.username, userDto.password);

            if (!result.Success)
            {
                return Ok(result);//reason for failure inside object, json return
            }

            string gettingRoleMessage;
            string selectedRole = _roleManager.GetRole(result.Roles, out gettingRoleMessage);
            if (!String.IsNullOrEmpty(gettingRoleMessage))
            {
                result.ErrorReason = "Role cannot be found for user: " + gettingRoleMessage;
                result.Success = false;
                return Ok(result);
            }

            if (!selectedRole.Contains("HQ"))
            {
                result.ErrorReason = "Only HQ roles have access to Administrative Application.";
                result.Success = false;
                result.Token = "";
                return Ok(result);
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(IGISConstants.JWTKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userDto.username),
                    new Claim(ClaimTypes.Role, result.Role)
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info (without password) and token to store client side
            return Ok(new ArcgisSignInResult
            {
                Success = true,
                Role = selectedRole,
                Token = tokenString, ExpireTicks = result.ExpireTicks
            });
            
        }
    }


}