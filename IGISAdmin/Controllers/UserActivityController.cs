﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IGISAccess.Models;
using IGISAdmin.ArcgisAccess;
using IGISAdmin.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IGISAdmin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserActivityController : ControllerBase
    {
        private IUserActivityService _userActivityService;

        public UserActivityController(IUserActivityService userActivityService)
        {
            _userActivityService = userActivityService;
        }

        [HttpGet("layers")]
        public IEnumerable<Layer> layers()
        {
            return _userActivityService.GetLayers();
        }

        [HttpGet("roles")]
        public IEnumerable<Role> roles()
        {
            return _userActivityService.GetRoles();
        }

        [HttpGet()]
        public IActionResult Index([FromQuery] UserActivityQueryString query)
        {
            try
            {
                return query.groupby == "layer"
                       ? Ok(_userActivityService.GetUserActivityByLayer(query.start, query.end, query.role, query.layer))
                       : Ok(_userActivityService.GetUserActivityByRole(query.start, query.end, query.role, query.layer));
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("roles/{role}/layers")]
        public IActionResult LayerBreakdown(string role, [FromQuery] UserActivityQueryString query)
        {
            try
            {
                var result = _userActivityService.GetLayerBreakdownByRole(query.start, query.end, role, "");
                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [HttpGet("roles/{role}/layers/{layer}")]
        public IActionResult LayerBreakdown(string role, string layer, [FromQuery]UserActivityQueryString query)
        {// TODO: mmk: not used?
            try
            {
                var result = _userActivityService.GetLayerBreakdownByRole(query.start, query.end, role, layer);
                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [HttpGet("users/{user}/layers")]
        public IActionResult LayerBreakdownByUser(string user, [FromQuery]UserActivityQueryString query)
        {
            try
            {
                var result = _userActivityService.GetLayerBreakdownByUser(query.start, query.end, user);
                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [HttpGet("layers/{layer}/roles")]
        public IActionResult RoleBreakdown(string layer, [FromQuery] UserActivityQueryString query)
        {
            try
            {
                var result = _userActivityService.GetRoleBreakdownByLayer(query.start, query.end, layer);
                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [HttpPost("insert")]
        // Receives input to insert into user activity table; date is of string format yyyy-mm-dd 
        public IActionResult InsertUserActivity(UserActivityInsertParam param)
        {
            try
            {
                var result = _userActivityService.InsertUserActivityRecord(param.username, param.role, param.layer, param.date, param.inserts, param.updates, param.deletes);
                return result >= 0 ? Ok() : (IActionResult)StatusCode(500, "");
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
        }
    }
}