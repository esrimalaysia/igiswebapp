using IGISAccess.ArcgisAccess;
using IGISAccess.Models;
using IGISAccess.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace IGISWebApp
{
    public class Startup
    {
        private readonly ILogger<Startup> _logger;
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public Startup(ILogger<Startup> logger, IConfiguration configuration, IWebHostEnvironment environment)
        {
            _logger = logger;
            Configuration = configuration;
            Environment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                //options.CheckConsentNeeded = context => false; //set to false: best for sessions to work
                options.Secure = CookieSecurePolicy.Always;// Environment.IsDevelopment()? CookieSecurePolicy.None : CookieSecurePolicy.Always;

                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                    builder =>
                    {
                        builder.AllowAnyOrigin();
                    });
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                //options.AccessDeniedPath = new PathString("/Account/AccessDenied");
                options.Cookie.Name = "EsriIGISWebApp";
                options.ExpireTimeSpan = TimeSpan.FromHours(1); //TimeSpan.FromMinutes(30);
                options.LoginPath = new PathString("/account/login");
                options.LogoutPath = new PathString("/account/logout");
                options.AccessDeniedPath = new PathString("/account/forbidden");
                // ReturnUrlParameter requires `using Microsoft.AspNetCore.Authentication.Cookies;`
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                //options.Events = new IgisCookieAuthenticationEvents();
            });

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            AppConfig appConfig = Configuration.GetSection("IGISConfig").Get<AppConfig>();
            IGISConfigAccess _igisConfig = new IGISConfigAccess();

            IGISConfig igisConfig = Environment.IsDevelopment() ? _igisConfig.GetConfigForDevelopment() : _igisConfig.GetConfig();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ICemsService, CemsService>(serviceProvider =>
            {
                return new CemsService(igisConfig);
            });
            services.AddSingleton<IArcGISRoleManager, ArcGISRoleManager>(serviceProvider =>
            {
                return new ArcGISRoleManager(igisConfig, appConfig.AppId);
            });
            services.AddSingleton<IArcgisSignInManager, ArcgisSignInManager>(serviceProvider =>
            {
                return new ArcgisSignInManager(igisConfig);
            });

            services.AddSingleton(appConfig);
            services.AddSingleton(igisConfig);

            // route options
            //services.AddRouting(options => options.AppendTrailingSlash = true);

            services.AddMemoryCache();

            services.AddSession();
            services.AddControllersWithViews().AddSessionStateTempDataProvider();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.None,
            });

            app.UseCors("AllowAllOrigins");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseRouting();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/");
                endpoints.MapControllerRoute(
                    name: "login",
                    pattern: "{controller=Account}/{action=Login}/");
            });
        }
    }
}
