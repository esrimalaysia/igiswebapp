﻿using IGISAccess.ArcgisAccess;
using IGISAccess.Models;
using IGISAccess.Services;
using IGISAccess.Utility;
using IGISAccess.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Security.Claims;

namespace IGISWebApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IGISConfig _igisConfig;
        private ICemsService _cemsService;
        private readonly IArcGISRoleManager _roleManager;
        private AppConfig _appConfig;

        public HomeController(AppConfig appConfig, IGISConfig igisConfig, ICemsService cemsService, IArcGISRoleManager roleManager, IHttpContextAccessor httpContextAccessor)
        {
            _igisConfig = igisConfig;
            _cemsService = cemsService;
            _roleManager = roleManager;
            _appConfig = appConfig;
            this._httpContextAccessor = httpContextAccessor;
        }

        [Authorize]
        public IActionResult Index()
        {
            string displayUrl = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(Request);
            if (displayUrl[displayUrl.Length - 1] != '/')
            {// makes sure main index page always has trailing slash
                return Redirect(displayUrl + '/');
            }

            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            
            IndexViewModel model = null;
            string indexViewModel = _httpContextAccessor.HttpContext.Session.GetString("IndexViewModel");
            if (!String.IsNullOrEmpty(indexViewModel))
            {
                model = JsonConvert.DeserializeObject<IndexViewModel>(indexViewModel);
            }
            else if (HttpContext.User.Identity != null)
            {
                ClaimsIdentity currentCookieData = HttpContext.User.Identity as ClaimsIdentity;
                if (!String.IsNullOrEmpty(currentCookieData.Name))
                {
                    long cookieExpiredTicks = Int64.Parse(currentCookieData.FindFirst(ClaimTypes.Expired).Value);
                    if (cookieExpiredTicks < Utils.GetJsTimestampNow(0))
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                    model = new IndexViewModel()
                    {
                        Username = currentCookieData.Name,
                        Token = currentCookieData.FindFirst(ClaimTypes.UserData).Value,
                        PortalToken = currentCookieData.FindFirst(ClaimTypes.AuthenticationInstant).Value,
                        RoleName = currentCookieData.FindFirst(ClaimTypes.Role).Value,
                        WebMapReference = currentCookieData.FindFirst(ClaimTypes.Sid).Value,
                        Expires = cookieExpiredTicks,
                        Password = SecureStringHelper.DecryptText(currentCookieData.FindFirst(ClaimTypes.Authentication).Value),
                        ArcgisServerUrl = _igisConfig.ArcgisSvrUrl,
                        ArcgisPortalUrl = _igisConfig.PortalUrl
                    };
                    Privilege priv = _roleManager.GetRolePrivilege(model.RoleName);
                    model.LocalAuthorities = String.Join(",",priv.LocalAuthorities);
                }
            }
            if (model == null)
            {
                return View();
            }
            else
            {
                model.UserActivityUrl = _igisConfig.InsertActivityUrl;
                model.LAQueryUrl = _igisConfig.LAQueryUrl;
                return View(model);
            }
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
