///////////////////////////////////////////////////////////////////////////
// Copyright © 2014 - 2017 Esri. All Rights Reserved.
//
// Licensed under the Apache License Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////

define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/array',
	'dojo/dom-attr',
    'dojo/Deferred',
    'esri/tasks/query',
    'esri/tasks/QueryTask',
    'jimu/utils',
    'jimu/dijit/Message'
  ],
  function(declare, lang, array, domAttr, Deferred, EsriQuery, QueryTask, jimuUtils, Message) {

    var SingleTaskClass = declare(null, {

      //public methods: 
	  
	  getLANames: function() {
        let webmaprole = sessionStorage.getItem("igiswebapprole");
		let laNames = null;

		if (webmaprole != "HQ_Edit" && webmaprole != "HQ_View") {			
            let laCsvList = sessionStorage.getItem("igiswebappla");
            let localAuthorities = laCsvList.split(',');
            let laName = localAuthorities.map(function (a) {
                return "LA_NAME = '" + a + "'";
            }); //within single quotes
            laNames = laName.join(" OR ");	
		}		
        return laNames;
      }
    });

    return SingleTaskClass;
  });