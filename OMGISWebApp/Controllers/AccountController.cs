﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IGISAccess.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using IGISAccess.ArcgisAccess;
using IGISWebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using IGISAccess.Utility;
using IGISAccess.ViewModels;

namespace IGISWebApp.Controllers
{
    [Route("account/[action]")]
    public class AccountController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IArcgisSignInManager _signInManager;
        private readonly ILogger _logger;
        private readonly IGISConfig _igisConfig;
        private readonly IArcGISRoleManager _roleManager;
        private readonly AppConfig _appConfig;

        public AccountController(AppConfig appConfig, IGISConfig igisConfig, IArcgisSignInManager signInManager,
             IArcGISRoleManager roleManager, ILogger<AccountController> logger, IHttpContextAccessor httpContextAccessor)
        {
            _signInManager = signInManager;
            _logger = logger;
            _igisConfig = igisConfig;
            _roleManager = roleManager;
            _appConfig = appConfig;
            this._httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        [ActionName("login")]
        [AllowAnonymous]
        //This is the method where users are directed to when to login page
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ActionName("login")]
        //This method is after users clicked login button
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            LoginViewModel modelIn = model;
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                ArcgisSignInResult result = await _signInManager.CheckPasswordSignInAsync(modelIn.Username, modelIn.Password, true);
                if (!result.Success)
                {
                    modelIn.LoginMessage = result.ErrorReason;
                    return View(modelIn);
                }
                else
                {
                    string gettingRoleMessage;
                    Privilege privilege = _roleManager.GetRolePrivilege(result.Roles, out gettingRoleMessage);
                    if (!String.IsNullOrEmpty(gettingRoleMessage))
                    {
                        modelIn.LoginMessage = gettingRoleMessage;
                        _logger.LogError(gettingRoleMessage);
                        return View(modelIn);
                    }

                    IndexViewModel viewModel = new IndexViewModel()
                    {
                        Username = model.Username,
                        Token = result.Token,
                        PortalToken = result.PortalToken,
                        RoleName = result.Role,
                        Expires = result.ExpireTicks,
                        Password = model.Password,
                        Region = privilege.Region,
                        WebMapReference = privilege.WebMapReference,
                        LocalAuthorities = String.Join(",", privilege.LocalAuthorities),
                        ArcgisServerUrl = _igisConfig.ArcgisSvrUrl,
                        ArcgisPortalUrl = _igisConfig.PortalUrl,
                        ManholeQueryUrl = _igisConfig.ManholeQueryUrl,
                        SewerlineQueryUrl1 = _igisConfig.SewerlineQueryUrl1,
                        SewerlineQueryUrl2 = _igisConfig.SewerlineQueryUrl2,
                        UpdateCEMSUrl = _igisConfig.UpdateCEMSUrl,
                        RestComplaintUrl = _igisConfig.RestComplaintUrl
                    };

                    List<Claim> claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, model.Username),
                        new Claim(ClaimTypes.Role, result.Role),
                        new Claim(ClaimTypes.UserData, result.Token),
                        new Claim(ClaimTypes.AuthenticationInstant, result.PortalToken),
                        new Claim(ClaimTypes.Sid, privilege.WebMapReference),
                        new Claim(ClaimTypes.Expired, result.ExpireTicks.ToString()),
                        new Claim(ClaimTypes.Authentication, SecureStringHelper.EncryptText(modelIn.Password))
                    };

                    // create identity
                    ClaimsIdentity identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    // create principal
                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                    var authProperties = new AuthenticationProperties { IsPersistent = true };

                    // sign-in
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, authProperties);

                    _logger.LogInformation("User logged in.");
                    // Call igis admin endpoint to insert Web Access info, fire and forget tasks
                    Task task = InsertActivityAsync(_igisConfig.InsertActivityUrl, model.Username, result.Role);
                    Task backgroundTask = task.ContinueWith(r => { });

                    if (returnUrl == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToLocal(returnUrl, viewModel);
                    }
                }
            }
            return View();
            // If we got this far, something failed, redisplay form
        }

        private async Task InsertActivityAsync(string igisInsertActivityUrl, string username, string role)
        {
            await IGISAdminAccess.InsertIgisAccessInfoAsync(_igisConfig.InsertActivityUrl, username, role);
        }

        private IActionResult RedirectToLocal(string returnUrl, IndexViewModel viewModel)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                if (returnUrl == "/")
                {
                    _httpContextAccessor.HttpContext.Session.SetString("IndexViewModel", JsonConvert.SerializeObject(viewModel));
                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
                else
                {
                    return Redirect(returnUrl);
                }
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        [HttpGet]
        [ActionName("logout")]
        [AllowAnonymous]
        public IActionResult Logout(string returnUrl = null)
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            _logger.LogInformation("User logged out.");
            ViewData["ReturnUrl"] = returnUrl;
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            _logger.LogInformation("User logged out.");
            return View();
        }

        [HttpPost]
        [ActionName("gettoken")]
        [AllowAnonymous]
        //This is the method where users can get new tokens (admin) for use
        public async Task<IActionResult> GetToken([FromBody] TokenCredentials cred)
        {
            string password = SecureStringHelper.DecryptText(cred.Password);
            ArcgisSignInResult result = await _signInManager.CheckPasswordSignInAsync(cred.Username, password, true);
            return Json(result);
        }

        public class TokenCredentials
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}